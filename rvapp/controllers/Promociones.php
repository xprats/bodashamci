<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promociones extends CI_Controller {

	protected $idioma = "es";

	public function __construct() {
		parent::__construct();
		if ($this->uri->segment(1) == "ca") { $this->idioma = $this->uri->segment(1); }
	}

	public function index($lang = "")	{
		$lang = strtolower($lang);
		if ($lang == "es" || $lang == "ca") {
			$this->cambiar_idioma($lang);
			
		} else {

			$this->lang->load("menu", $this->idioma);
			$this->lang->load("promociones", $this->idioma);

			$datos = array(	"idioma"			=>	$this->idioma,
							"css_pagina" 		=> 	"promociones",
							"titulo" 			=> 	"",
							"meta_description" 	=> 	"");

			$this->load->view('genericos/header', $datos);

				$this->load->view('genericos/barra_superior', $datos);

				$this->load->view('genericos/menu', $datos);

				$this->load->view('promociones/promociones');

			$this->load->view('genericos/footer');
		}
	}

	public function ver($id, $lang = "")	{
		if ($id == "" || $id == "ca" || $id == "es") {
			$this->index($lang);
		} else {
			$lang = strtolower($lang);
			if ($lang == "es" || $lang == "ca") {
				$this->cambiar_idioma($lang, $id);
				
			} else {

				$this->lang->load("menu", $this->idioma);
				$this->lang->load("promociones", $this->idioma);

				$datos = array(	"idioma"			=>	$this->idioma,
								"css_pagina" 		=> 	"promocion",
								"titulo" 			=> 	"",
								"meta_description" 	=> 	"",
								"numero"			=> 	$id);

				$this->load->view('genericos/header', $datos);

					$this->load->view('genericos/barra_superior', $datos);

					$this->load->view('genericos/menu', $datos);

					$this->load->view('promociones/promocion', $datos);

				$this->load->view('genericos/footer');
			}
		}
	}

	public function cambiar_idioma($lang, $maxi = false) {

		if ($lang == "es" || $lang == "ca") {
			$this->idioma = $lang; 
			$_SESSION["idioma"] = $lang;

			$this->lang->load("menu", $this->idioma);
			$this->lang->load("promociones", $this->idioma);


			$prefijo = "";
			if ($lang == "es") {}
			if ($lang == "ca") { $prefijo = "ca/"; }

			if ($maxi === false) {
				$sufijo = lang("m_u_promociones");
			} else {
				$sufijo = lang("promo".intval($maxi)."_url");
			}

			$data["destino"] = base_url().$prefijo.$sufijo;
			$this->load->view('genericos/jump', $data);

		}
	}
}