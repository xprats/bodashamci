<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	protected $idioma = "es";

	public function __construct() {
		parent::__construct();
		if ($this->uri->segment(1) == "ca") { $this->idioma = $this->uri->segment(1); }
	}

	public function index($lang = "")	{
		$lang = strtolower($lang);
		if ($lang == "es" || $lang == "ca") {
			$this->cambiar_idioma($lang);
			
		} else {

			$this->lang->load("menu", $this->idioma);
			$this->lang->load("contacto", $this->idioma);

			$datos = array(	"idioma"			=>	$this->idioma,
							"css_pagina" 		=> 	"contacto",
							"titulo" 			=> 	"",
							"meta_description" 	=> 	"");

			$this->load->view('genericos/header', $datos);
			$this->load->view('genericos/barra_superior', $datos);
			$this->load->view('genericos/menu', $datos);
			$this->load->view('contacto/contacto', $datos);
			$this->load->view('genericos/mapa');
			$this->load->view('genericos/footer');
		}
	}
	
	public function lanzar() {

		// echo die("<pre>post: ".print_r($_POST, true)."</pre>");

		$this->form_validation->set_rules("nombre",					lang("nombre"), 				'trim|required');
		$this->form_validation->set_rules("email", 					lang("email"), 					'trim|required|valid_email');
		$this->form_validation->set_rules("telefono", 				lang("telefono"), 				'trim|required');
		$this->form_validation->set_rules("comentarios", 			lang("comentarios"),			'trim');
		$this->form_validation->set_rules("privacidad", 			lang("privacidad"),				'trim|required');
		$this->form_validation->set_rules("idioma",	 				lang("idioma"),					'trim|required');
		$this->form_validation->set_rules("g-recaptcha-response",   "recaptcha",                 "required|trim|xss_clean|callback_check_recaptcha");

		if($this->form_validation->run() == FALSE) {
			
			$this->index();

		} else {

			$this->load->library('email');
			$this->email->clear(true);
			$this->email->initialize();
			$this->idioma = $_POST["idioma"];

			$from_email		= "comercial@hotelametllamar.com";
			$from_nombre 	= "Bodas Hotel Ametlla Mar";

			$reply_email	= "comercial@hotelametllamar.com";
			$reply_nombre 	= "Bodas Hotel Ametlla Mar";

			$cc_email		= "comercial@hotelametllamar.com";
			$bcc_email		= "web@gruprv.es";

			$titulo_email	 = "Formulario contacto Bodas Hotel Ametlla Mar";
			$cuerpo_email	 = "<b>Nombre:</b> ".$_POST["nombre"]."<br/>";
			$cuerpo_email	.= "<b>Email:</b> ".$_POST["email"]."<br/>";
			$cuerpo_email	.= "<b>Telefono:</b> ".$_POST["telefono"]."<br/>";
			$cuerpo_email	.= "<b>Comentarios:</b> ".$_POST["comentarios"]."<br/>";
			$cuerpo_email	.= "<b>Idioma:</b> ".$this->idioma."<br/>";

			$this->email->subject($titulo_email);
			$this->email->message($cuerpo_email);
			$this->email->set_alt_message($titulo_email);            
			$this->email->from($from_email, $from_nombre);
			$this->email->reply_to($reply_email, $reply_nombre);

			// send copy to customer
			$array_to = array();
			$array_to[] = $_POST["email"];
			$this->email->to($array_to);

			if ($cc_email != "") {
				$array_cc = array();
				$array_cc[] = $cc_email;                
				$this->email->cc($array_cc);
			}

			if ($cc_email != "") {
				$array_bcc = array();
				$array_bcc[] = $bcc_email;                
				$this->email->bcc($array_bcc);
			}

			try {
				$resultado = $this->email->send();

				if(!$resultado) {
					$titulo_resultado = "Envio ERRONEO: ".$this->email->print_debugger();
					$texto_error = $this->email->print_debugger();
					$estado_error = 0;

					$this->index();

				} else {
					$titulo_resultado = "Envio CORRECTO!";
					$texto_error = $this->email->print_debugger();
					$estado_error = 1;

					$this->lang->load("menu", $this->idioma);

					$prefijo = "";
					if ($this->idioma == "es") {}
					if ($this->idioma == "ca") { $prefijo = "ca/"; }

					redirect(base_url().$prefijo.lang("m_u_contacto_ok"),"refresh");
				}
			} catch (Exception $e) {
				$titulo_resultado = "Envio ERRONEO: ".$e->getMessage();
				$texto_error = $e->getMessage();
				$estado_error = 0;            

				$this->index();
			}

			// echo $estado_error." - ".$titulo_resultado.": ".$texto_error;
		}
	}

	public function ok()	{

		$this->lang->load("menu", $this->idioma);
		$this->lang->load("contacto", $this->idioma);

		$datos = array(	"idioma"			=>	$this->idioma,
						"css_pagina" 		=> 	"contacto",
						"titulo" 			=> 	"",
						"meta_description" 	=> 	"");

		$this->load->view('genericos/header', $datos);

			// $this->load->view('genericos/barra_superior', $datos);

			// $this->load->view('genericos/menu', $datos);

			$this->load->view('contacto/contactoOK', array("contactoOK" => true));

		$this->load->view('genericos/footer');
	}

	public function okmailing()	{

		$this->lang->load("menu", $this->idioma);
		$this->lang->load("contacto", $this->idioma);

		$datos = array(	"idioma"			=>	$this->idioma,
						"css_pagina" 		=> 	"contacto",
						"titulo" 			=> 	"",
						"meta_description" 	=> 	"");

		$this->load->view('genericos/header', $datos);

			// $this->load->view('genericos/barra_superior', $datos);

			// $this->load->view('genericos/menu', $datos);

			$this->load->view('contacto/contactoOKmailing', array("contactoOK" => true));

		$this->load->view('genericos/footer');
	}

	public function okadwords()	{

		$this->lang->load("menu", $this->idioma);
		$this->lang->load("contacto", $this->idioma);

		$datos = array(	"idioma"			=>	$this->idioma,
						"css_pagina" 		=> 	"contacto",
						"titulo" 			=> 	"",
						"meta_description" 	=> 	"");

		$this->load->view('genericos/header', $datos);

			// $this->load->view('genericos/barra_superior', $datos);

			// $this->load->view('genericos/menu', $datos);

			$this->load->view('contacto/contactoAdwords', array("contactoOK" => true));

		$this->load->view('genericos/footer');
	}

	public function cambiar_idioma($lang) {

		if ($lang == "es" || $lang == "ca") {
			$this->idioma = $lang; 
			$_SESSION["idioma"] = $lang;

			$this->lang->load("menu", $this->idioma);

			$prefijo = "";
			if ($lang == "es") {}
			if ($lang == "ca") { $prefijo = "ca/"; }

			$data["destino"] = base_url().$prefijo.lang("m_u_contacto");
			$this->load->view('genericos/jump', $data);

		}
	}
	
	public function check_recaptcha($valor) {

        /* despues de validar el formulario miramos el captcha es correcto. */
        $this->load->library('Soporte_recaptcha');
            
        $recaptcha_ok = $this->soporte_recaptcha->check_recaptcha($valor);	

        return $recaptcha_ok;
        
	}
}