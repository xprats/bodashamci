<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Legal extends CI_Controller {

	protected $idioma = "es";

	public function __construct() {
		parent::__construct();
		if ($this->uri->segment(1) == "ca") { $this->idioma = $this->uri->segment(1); }
	}

	public function index($lang = "")	{
		$lang = strtolower($lang);
		if ($lang == "es" || $lang == "ca") {
			$this->cambiar_idioma($lang);
			
		} else {

			$this->lang->load("menu", $this->idioma);
			$this->lang->load("legal", $this->idioma);

			$datos = array(	"idioma"			=>	$this->idioma,
							"css_pagina" 		=> 	"nota-legal",
							"titulo" 			=> 	"",
							"meta_description" 	=> 	"");

			$this->load->view('genericos/header', $datos);

				$this->load->view('genericos/barra_superior', $datos);

				$this->load->view('genericos/menu', $datos);

				$this->load->view('legal/legal');

			$this->load->view('genericos/footer');
		}
	}

	public function cambiar_idioma($lang) {

		if ($lang == "es" || $lang == "ca") {
			$this->idioma = $lang; 
			$_SESSION["idioma"] = $lang;

			$this->lang->load("menu", $this->idioma);

			$prefijo = "";
			if ($lang == "es") {}
			if ($lang == "ca") { $prefijo = "ca/"; }

			$data["destino"] = base_url().$prefijo.lang("m_u_nota_legal");
			$this->load->view('genericos/jump', $data);

		}
	}
}