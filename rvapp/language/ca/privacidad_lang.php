<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			        = "Política de privacitat - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/politica-de-privacitat";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."politica-de-privacidad";

	$lang["meta_description"]       = "Política de privacitat. Ens prenem molt seriosament la seguretat de tots els nostres clients, i les seves dades personals no son una excepció.";

	$lang["pri_h1"]			        = "Política de Privacitat";
	
	$lang["pri_txt_1"]		        = "A RV Hotels ens prenem molt seriosament la seguretat de tots els nostres clients i les seves dades personals no en són una excepció.";
	$lang["pri_txt_2"]				= "<strong>RV Hotels Turistics, S.L.U.</strong> protegeix les dades personals segons el que s'estableix en el Reglament (UE) 2016/679 del Parlament Europeu i del Consell, de 27 d'abril de 2016, relatiu a la protecció de les persones físiques pel que fa al tractament de dades personals i a la lliure circulació d'aquestes dades, amb l'objectiu que es respectin les llibertats i els drets fonamentals de les persones a qui pertanyen aquestes dades.";
	$lang["pri_txt_3"]				= "Les vostres dades seran tractades amb la màxima confidencialitat per tal de gestionar la nostra relació comercial. Les dades es conservaran fins que se'n sol·liciti la supressió a l'adreça <a href=\"mailto:lopd@gruprv.es\">lopd@gruprv.es</a>, o fins que ho estableixi una disposició legal. Podeu presentar una reclamació davant l'autoritat de control competent (<a href=\"https://www.agpd.es\" target=\"_blank\">www.agpd.es</a>) si considereu que el tractament no s'ajusta a la normativa vigent.";
	$lang["pri_txt_4"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> està legitimada per a tractar les vostres dades perquè són necessàries per a prestar el servei sol·licitat o pel consentiment que ens atorgeu en els diferents formularis de contacte del web.";
	$lang["pri_txt_5"]		        = "Les dades no es cediran a tercers, tret en els casos en què hi hagi una obligació legal.";
	$lang["pri_txt_6"]		        = "Per tal d'exercir els drets d'accés, rectificació, supressió i portabilitat de les dades, i la limitació o oposició al tractament, cal dirigir-se per escrit a <a href=\"mailto:lopd@gruprv.es\">lopd@gruprv.es</a>.";
	$lang["pri_txt_7"]		        = "Els usuaris, mitjançant el marcatge de les caselles corresponents i l'entrada de dades en els camps marcats amb un asterisc (*) en el formulari de contacte o presentats en formularis de descàrrega, accepten expressament i de manera lliure i inequívoca que les seves dades són necessàries perquè el prestador atengui la seva petició; no obstant això, és voluntària la inclusió de dades en la resta de camps. L'usuari garanteix que les dades personals facilitades són vertaderes i es fa responsable de comunicar-ne qualsevol modificació.";
	$lang["pri_txt_8"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> compleix totes les disposicions de les normatives per al tractament de les dades personals de les quals és responsable, així com els principis descrits en l'article 5 del RGPD, pels quals són tractades de manera lícita, fidel i transparent en relació a la persona interessada i adequades, pertinents i limitades al que és necessari d'acord amb la finalitat per a la qual són tractades.";
	$lang["pri_txt_9"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> garanteix que ha implementat polítiques tècniques i organitzatives apropiades per tal d'aplicar les mesures de seguretat que estableix el RGPD amb la finalitat de protegir els drets i les llibertats dels usuaris i els ha comunicat la informació adequada perquè puguin exercir-los.";
	$lang["pri_txt_10"]		        = "<strong>Clàusula addicional per a la recepció de CV i informació sobre llocs de treball i vacants.</strong>";
	$lang["pri_txt_11"]		        = "Tractem la informació que ens faciliteu amb la finalitat de mantenir-vos informat de les diferents vacants a llocs de treball que tinguin lloc en la nostra organització.";
	$lang["pri_txt_12"]		        = "Les dades proporcionades es conservaran fins a l'adjudicació d'un lloc de treball o fins que exerciu el vostre dret de cancel•lació i, per tant, teniu dret a accedir a les vostres dades personals, rectificar-ne les dades inexactes o sol•licitar-ne la supressió quan les dades ja no siguin necessàries. Les dades no se cediran a tercers.";
	$lang["pri_txt_13"]		        = "Hem adoptat totes les mesures de seguretat establertes en la legislació actual sobre protecció de dades. Podeu exercir en tot moment els drets d'accés, rectificació, supressió i portabilitat de les vostres dades, i la limitació o oposició al tractament, mitjançant un escrit dirigit a <a href=\"mailto:lopd@gruprv.es\">lopd@gruprv.es</a>. També teniu dret a presentar una reclamació davant l'autoritat de control competent (<a href=\"https://www.agpd.es\" target=\"_blank\">www.agpd.es</a>) si considereu que el tractament no s'ajusta a la normativa vigent.";

	$lang["pri_txt_14"]		        = "Adreça";
	$lang["pri_txt_15"]		        = "Telèfon";

	$lang["pri_txt_16"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong>, a través del seu web, utilitzarà cookies quan l'usuari navegui pel lloc web <a href=\"http://www.hotelametllamar.com\" target=\"_blank\">www.hotelametllamar.com</a>. Les cookies s'associen únicament amb un usuari anònim i el seu ordinador, i no proporcionen referències que permetin deduir-ne el nom o els cognoms. Gràcies a les cookies és possible reconèixer els usuaris registrats després que s'hi hagin registrat la primera vegada, sense que s'hi hagin de registrar en cada visita per accedir a les àrees i els serveis que se'ls reserva de manera exclusiva. L'usuari té l'opció d'impedir la generació de cookies mitjançant la selecció de l’opció corresponent al seu navegador.";

	$lang["pri_txt_17"]		        = "Tot usuari registrat pot, en qualsevol moment, exercir el dret a accedir a les seves dades de caràcter personal subministrades al web <a href=\"http://www.hotelametllamar.com\" target=\"_blank\">www.hotelametllamar.com</a>, rectificar-les o, si escau, cancel•lar-les, mitjançant una comunicació escrita dirigida a <strong>RV Hotels Turistics, S.L.U.</strong>, C/. Diputació, 238. Entl. 3a, Barcelona (08007), esta comunicación también podrá ser realizada mediante el envío de un correo electrónico a la dirección: <a href=\"mailto:info@rvhotels.es\">info@rvhotels.es</a>.";


    $lang[""]	= "";
    
?>