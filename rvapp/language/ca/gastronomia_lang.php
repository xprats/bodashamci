<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			       = "Menús per casaments a la costa de Tarragona - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/gastronomia-espais";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."gastronomia-espacios";

	$lang["meta_description"]      = "Casa't a la platja i celebra el teu banquet als nostres salons de noces davant del mar. Posem a la vostra disposició un entorn privilegiat i una excel·lent gastronomia.";

	$lang["gas_h1"]			       = "Gastronomia i espais";
	$lang["gas_h2"]			       = "Sales grans al restaurant de l'hotel i els millors plats de la nostra terra";
	$lang["gas_h3"]			       = "El millor entorn possible";
	$lang["gas_subtext"]	       = "<p>Tant a les sales com als jardins gaudireu d'<strong>un lloc privilegiat per celebrar-hi la vostra unió</strong>.</p>
									<p>L'<strong>Hotel Ametlla Mar 4*</strong> ho posa tot <strong>al servei dels nuvis</strong>.</p>";
	$lang["gas_mas_info"]	       = "Més informació";
	$lang["gas_menus"]		       = "Descarregar menús";

	$lang["gas_aperitivos_h4"]	   = "Aperitius";
	$lang["gas_aperitivos_desc"]   = "Gaudiu d'un saborós i extens aperitiu, envoltats de piscines i jardins davant del mar.";

	$lang["gas_gastronomia_h4"]	   = "Excel·lent gastronomia";
	$lang["gas_gastronomia_desc"]  = "Un gran equip de professionals, amb una àmplia experiència en banquets i dirigits pel Xef Victor Buera, està a la  vostra disposició perquè pugueu gaudir de la gastronomia més selecta feta amb productes de primera qualitat i locals, de les Terres de l'Ebre.";
	$lang["gas_gastronomia_desc2"] = "El millor peix fresc i les excel·lents carns, fruites i verdures.";

	$lang["gas_banquetes_h4"]	   = "Sala de banquets";
	$lang["gas_banquetes_desc"]	   = "Un meravellosa sala de vidre i fusta amb vistes al mar i als jardins.";
	$lang["gas_banquetes_desc2"]   = "La capacitat màxima de la sala és de <strong>350 comensals</strong>.";
	$lang["gas_galeria"]	       = "Veure galeria";

	$lang["gas_disco_h4"]	       = "Què no pari la disco!";
	$lang["gas_disco_desc"]	       = "Disposem d'un espai exclusiu per a ballar, on podreu allargar la festa sense límit horari.";
	$lang["gas_disco_desc2"]	   = "No oblideu que els vostres convidats poden quedar-se a dormir a l'hotel.";
				
	$lang["gas_galeria_h3"]	       = "L'espai ideal per al vostre dia";
	$lang["gas_galeria_desc"]	   = "La platja, els jardins, les piscines, les sales... una combinació ideal per a un dia inoblidable.";
	/* / textos para la visualizacion del listado de mantenimientos */
	
    $lang[""]	= "";
?>