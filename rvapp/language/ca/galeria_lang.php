<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			       = "Galeria - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/galeria";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."galeria";

	$lang["meta_description"]      = "Fotos de casaments a la platja. Gaudiu de la galeria d'imatges de parelles reals que han celebrat el seu casament a la platja a l'Hotel Ametlla Mar 4*. Ells ja ho han fet realitat.";

	$lang["g_h3"]		           = "Ells ja l'han fet realitat!";
    $lang["g_parrafo"]	           = "<p>Totes les fotos que apareixen al web corresponen a <strong>parelles reals que han celebrat la seva boda a l'Hotel Ametlla Mar 4*</strong>. Exposar-hi fotos reals és, al nostre parer, <strong>la millor manera de mostrar el nostre estil</strong> i d'explicar com entenem les bodes i com les organitzem.</p>
							<p><strong>Moltíssimes gràcies a totes les parelles que han confiat en nosaltres, perquè junts hem construït el vostre somni</strong>.</p>";

	$lang["g_cat_todas"]		   = "Totes";
	$lang["g_cat_ceremonia"]	   = "Cerimònia";
	$lang["g_cat_hotel"]		   = "Hotel";
	$lang["g_cat_salones"]		   = "Salons";
	$lang["g_cat_gastronomia"]	   = "Gastronomia";
	$lang["g_cat_novios"]		   = "Nuvis";
	$lang["g_cat_detalles"]		   = "Detalls";

    $lang[""]	= "";
    
?>