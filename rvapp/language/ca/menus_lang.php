<?php
	
	/* estamos en idioma -- CA -- */

    $lang["menus_titulo"]		= "Descobriu els nostres menús";

	/* menu 1: */
		$lang["menu1_titulo"]	= "MENÚ <span class=\"title-clicker\">La Cala</span>";
		$lang["menu1_info1"]	= "<strong>AMB ALLOTJAMENT GRATUÏT</strong><br>PER ALS TEUS CONVIDATS";
		$lang["menu1_info2"]	= "<span style=\"color:#0440AA\"><strong>DIVENDRES I DIUMENGES</strong></span>";
		$lang["menu1_desde"]	= "<span class=\"clicker\">des de</span> <span class=\"amarillo\">114€</span>";
		$lang["menu1_link_menu"]= base_url().'menus/menu_la_cala_CA.pdf';
	/* / menu 1 */

	/* menu 2: */
		$lang["menu2_titulo"]	= "MENÚ <span class=\"title-clicker\">Aguamarina</span>";
		$lang["menu2_info1"]	= "<span style=\"color:#00A5DF\"><strong>DISSABTES</strong></span>";
		$lang["menu2_info2"]	= "<span class=\"clicker todo-inc\">Tot inclòs</span>";
		$lang["menu2_desde"]	= "<span class=\"clicker\">des de</span> <span class=\"amarillo\">94€</span>";
		$lang["menu2_link_menu"]= base_url().'menus/menu_aguamarina_CA.pdf';
	/* / menu 2 */

	$lang[""]	= "";
	
?>