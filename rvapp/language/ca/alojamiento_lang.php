<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			  = "Hotel a la platja per Cerimònies - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/allotjament";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."alojamiento";


	$lang["meta_description"] = "Celebreu la vostra cerimònia civil a la platja, amb banquet i allotjament per a tots els convidats, tot al mateix espai. A l'Hotel Ametlla Mar 4* podreu allargar la boda tot el cap de setmana.";

	$lang["alo_h1"]			  = "Boda amb allotjament per als convidats";
	$lang["alo_h2"]			  = "Es poden quedar a dormir a l'Hotel Ametlla Mar 4* i gaudir fins al final de la festa";
	$lang["alo_h3"]			  = "Que no s'acabi la festa!";
	$lang["alo_subtext1"]	  = "<p>A l'<strong>Hotel Ametlla Mar 4*</strong>, podreu allotjar tots els vostres convidats i allargar aií la boda tot el cap de setmana.</p>
								<p>Que no us amoïni la tornada a casa: el nostre";
	$lang["alo_subtext_a"]	  = "Menú La Cala";
	$lang["alo_subtext2"]	  = "inclou en el preu <strong>l'allotjament per a totes les persones que us acompanyin</strong>.";
	$lang["alo_mas_info"]	  = "Més informació";
	$lang["alo_menus"]		  = "Descarregar menús";

	$lang["alo_hotel_h4"]	  = "Un hotel 4* per als convidats";
	$lang["alo_hotel_desc"]	  = "L'Hotel Ametlla Mar 4* està situat davant d'una meravellosa cala, al sud de la Costa Daurada.";
	$lang["alo_hotel_desc2"]  = "Disposa de les millors i més completes instal·lacions que us permetran descansar després de la gran festa.";

	$lang["alo_suite_h4"]	  = "El nostre regal: La Suite Nupcial";
	$lang["alo_suite_desc"]	  = "Podreu gaudir de la vostra nit de noces en la inigualable Suite Nupcial amb vistes d'una bellesa colpidora. Per sentir-vos com els prínceps de les \"Mil i una nits\".";
	$lang["alo_suite_desc2"]  = "Perquè <strong>desitgem que compteu amb nosaltres</strong> per complir els vostres somnis.";
	$lang["alo_galeria"]	  = "Veure galeria";

	$lang["alo_galeria_h3"]	  = "Gaudiu de la nostra Suite Nupcial i sentiu-vos com prínceps";
	$lang["alo_galeria_desc"] = "Aquestes fotos són de la nostra Suite i els seus espais.";
	
	$lang[""]	= "";
    
?>