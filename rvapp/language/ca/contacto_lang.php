<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			       = "Contacte - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/contacte";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."contacto";

	$lang["meta_description"]      = "Podeu complir el vostre somni de casar-vos a la platja. Contacteu amb nosaltres i convertiu el vostre casament a la platja en un esdeveniment únic. Us ajudem a complir-ho.";

	$lang["c_h1"] 	               = "Sí, volem!";
	$lang["c_h2"]	               = "Contacteu amb nosaltres mitjançant el formulari i us informarem sense compromís. O si ho preferiu, truqueu-nos directament.";
	$lang["c_h3"]	               = "Volem ajudar-vos a fer realitat el vostre somni!";
	$lang["c_nombre"]	           = "Nom";
	$lang["c_email"]	           = "E-Mail";
	$lang["c_telefono"]	           = "Telèfon";
	$lang["c_comentarios"]	       = "En què podem ajudar-vos?";
	// $lang["c_privacidad_1"]	   = "He leído y acepto la <a href=\"".base_url().$lang.lang("m_u_privacidad")."\">política de privacidad</a>";
	$lang["c_privacidad_1"]	       = "He llegit i accepto la ";
	$lang["c_privacidad_2"]	       = "política de privacitat";
	/* / textos para la visualizacion del formulario de contacto */

	/* textos para la visualizacion de la confirmacion de envio */ 
	$lang["c_ok_h2"]	           = "<strong>Gràcies per escriure'ns</strong>,<br /> estem desitjant organitzar el vostre gran dia.<br /><br />En breu ens posarem en contacte amb vosaltres.";
	$lang["c_ok_volver"]= "Tornar a l'inici";

	/* textos para la visualizacion ok mailing */ 
	$lang["c_ok_mailing_h1"] 	               = "Gràcies!";
	$lang["c_ok_mailing_h2"]	           = "Hem rebut la vostra petició,<br /> <strong>estem desitjant vuere-us a la fira!</strong><br /><br />En breu ens posarem en contacte amb vosaltres.";
	/* textos para la visualizacion de la confirmacion de envio */ 

    $lang[""]	= "";
?>