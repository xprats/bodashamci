<?php
	
	/* estamos en idioma -- CA -- */


	$lang["n_preguntas"] = 14;

	/* textos para la visualizacion del listado de mantenimientos */
	$lang["titulo"]			       = "Preguntes freqüents - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/preguntes-frequents";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."preguntas-frecuentes";

	$lang["meta_description"]      = "A l'hora d'organitzar un casament a la platja poden sorgir molts dubtes. Et volem ajudar a resoldre totes les preguntes. Nosaltres t'ho expliquem tot.";

	$lang["p_h1"]				   = "Preguntes freqüents";
	$lang["p_h2"]				   = "Preparar una boda requereix molt de temps i molta informació que algú ha de proporcionar-vos. Nosaltres us ho  expliquem tot.";
	
	$lang["p_p1"]				   = "És legal la cerimònia civil?";
	$lang["p_r1"]				   = "Sí, la cerimònia civil a la platja que organitzem a l'Hotel Ametlla Mar 4* pot ser vàlida jurídicament. Només heu de demanar al vostre ajuntament el trasllat de l'expedient.";

	$lang["p_p2"]				   = "Quant costa la cerimònia civil a la platja?";
	$lang["p_r2"]				   = "Si trieu el menú Aguamarina, la cerimònia civil és gratuïta. Si escolliu el menú La Cala, la cerimònia civil té un suplement.";

	$lang["p_p3"]				   = "Podem decorar la cerimònia civil al nostre gust?";
	$lang["p_r3"]				   = "Sí. A partir de la nostra proposta de decoració, podeu fer canvis i personalitzar la cerimònia civil a la platja.";

	$lang["p_p4"]				   = "Podem decorar la sala i els altres espais?";
	$lang["p_r4"]				   = "Sí. Podeu canviar els centres de taula, les tovalles i tovallons, decorar la zona d'aperitiu amb torxes... És la vostra boda i podeu decorar-la com vulgueu, al vostre estil.";

	$lang["p_p5"]				   = "Està el DJ inclòs al preu?";
	$lang["p_r5"]				   = "Sí. Està inclòs per a l'aperitiu, el banquet i 3 hores de barra lliure en bodes a partir de 95 convidats, si escolliu el menú La Cala. Si treu el menú Aguamarina, el DJ està inclòs a partir de 80 comensals.";

	$lang["p_p6"]				   = "Podem escollir la música?";
	$lang["p_r6"]				   = "I tant! La música és molt important i també parla de vosaltres. Us reunireu abans de la boda amb el nostre DJ per escollir la música que voleu.";

	$lang["p_p7"]				   = "Oferiu servei de wedding planner?";
	$lang["p_r7"]				   = "Sí. El servei de wedding planner està inclòs en el preu. Posem al vostre servei un gran equip de professionals que us ajudaran a preparar aquest gran dia. Durant la vostra boda estaran pendents de tot, perquè pugueu gaudir de cada moment. No us haureu de preocupar de res.";

	$lang["p_p8"]				   = "Podem allargar la barra lliure tot el que vulguem?";
	$lang["p_r8"]				   = "Sí. L'hotel disposa de discoteca privada per a vosaltres sense límit horari.";

	$lang["p_p9"]				   = "Està l'IVA inclòs en el preu?";
	$lang["p_r9"]				   = "A tots els preus cal sumar-hi el 10 % d'IVA.";

	$lang["p_p10"]				   = "Teniu servei de candy bar o ressopó?";
	$lang["p_r10"]				   = "Sí. Oferim un ampli assortiment de propostes per al ressopó i el servei de candy bar per endolcir la festa als vostres convidats. El servei de ressopó està inclòs en el preu en el menú Aguamarina.";

	$lang["p_p11"]				   = "Hi ha servei de monitor infantil?";
	$lang["p_r11"]				   = "Sí. Una boda és una festa per a tots. És molt important que els nens no s'avorreixin i que els pares es relaxin i gaudeixin de la boda. Disposem de monitors qualificats que tindran cura dels infants durant tot l'esdeveniment, així com de magnífiques instal·lacions d'oci infantils que estaran al seu servei.";

	$lang["p_p12"]				   = "Podem portar el nostre pastís nupcial?";
	$lang["p_r12"]				   = "Sí. El podeu portar vosaltres, si així ho voleu.";

	$lang["p_p13"]				   = "Poden dormir a l'hoptel els nostres convidats?";
	$lang["p_r13"]				   = "Sí. Amb el menú La Cala, l'allotjament dels convidats és gratuït. Si escolliu el menú Aguamarina, podeu disposar d'interessants ofertes i promocions per a l'allotjament dels vostres convidats.";

	$lang["p_p14"]				   = "Hi ha habitació per als nuvis?";
	$lang["p_r14"]				   = "Sí. Tots els nostres menús inclouen la fantàstica Suite per a la nit de noces. És el nostre regal per als noucasats.";

    $lang[""]	= "";
?>