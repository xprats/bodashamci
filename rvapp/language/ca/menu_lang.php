<?php
	
	/* estamos en idioma -- CA -- */

    $lang["m_u_ceremonia"]      = "casament-a-la-platja";
    $lang["m_t_ceremonia"] 	    = "Cerimonia<span>a la platja</span>";

    $lang["m_u_alojamiento"]    = "allotjament";
    $lang["m_t_alojamiento"]    = "Allotjament<span>per a invitats</span>";

    $lang["m_u_gastronomia"] 	= "gastronomia-espais";
    $lang["m_t_gastronomia"] 	= "Gastronomia<span>espais</span>";

    $lang["m_u_galeria"] 	    = "galeria";
    $lang["m_t_galeria"] 	    = "Galeria<span>de fotos</span>";

    $lang["m_u_promociones"] 	= "promocions";
    $lang["m_t_promociones"] 	= "Promocions<span>ofertes</span>";

    $lang["m_u_opiniones"] 	    = "opinions-de-parelles";
    $lang["m_t_opiniones"] 	    = "Opinions<span>de parelles</span>";

    $lang["m_u_preguntas"] 	    = "preguntes-frequents";
    $lang["m_t_preguntas"] 	    = "Preguntes<span>frequents</span>";

    $lang["m_u_contacto"] 	    = "contacte";
    $lang["m_t_contacto"] 	    = "Informació<span>contacte</span>";

    $lang["m_u_contacto_ok"] 	= "gracies-per-contactar-amb-nosaltres/";    

    $lang["m_u_nota_legal"] 	= "nota-legal";
    $lang["m_t_nota_legal"] 	= "Avís legal";

    $lang["m_u_privacidad"] 	= "politica-de-privacitat";
    $lang["m_t_privacidad"] 	= "Política de privacitat";
    $lang["m_home"] 	        = "";

    $lang[""]	= "";
    
?>