<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			     = "Especialistes en casaments a la platja - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url();

	$lang["meta_description"]    = "Celebra la boda a la platja que sempre havies somniat a l'Hotel Ametlla Mar 4*, a la Costa Daurada. Un casament únic a primera línea de mar i amb un entorn inigualable.";

	$lang["hom_h1"]			     = "Bodes a la platja Hotel Ametlla Mar 4*";
	$lang["hom_h2"]			     = "Feu realitat el vostre somni. Caseu-vos a la platja!";
	$lang["hom_h3"]			     = "Ho tenim tot a punt!";

	$lang["hom_subtext"]	     = "Si alguna vegada heu somiat a celebrar <strong>la vostra boda a la platja</strong>, davant del mar, l'Hotel Ametlla Mar 4* és <strong>l'espai perfecte per a fer realitat el vostre somni</strong>.";

	$lang["hom_btn_casar_playa"] = "Volem casar-nos a la platja!";

	$lang["hom_tit_box1"]	     = "Cerimònia civil a la platja";
	$lang["hom_txt_box1"]	     = "Caseu-vos a la platja, en un entorn únic i privilegiat davant del mar.";
	$lang["hom_tit_box2"]	     = "Personalitzeu la vostra boda";
	$lang["hom_txt_box2"]	     = "Cada boda és diferent perquè no hi ha dues parelles iguals. <strong>Si ho heu somiat, podem organizar-ho.</strong>";
	$lang["hom_tit_box3"]	     = "Allotjament per als vostres convidats";
	$lang["hom_txt_box3"]	     = "Feu de la vostra boda una festa de cap de setmana al nostre <strong>hotel de 4 estrelles.</strong>";
	$lang["hom_tit_box4"]	     = "La gastronomia més selecta";
	$lang["hom_txt_box4"]	     = "De la mà del Xef Victor Buera, amb <strong>productes de primera qualitat i de la zona</strong>, les Terres de l'Ebre.";

	$lang["hom_mas_info"]	     = "Més informació";

	$lang["hom_txt_blue_box"]    = "Un gran equip de professionals amb àmplia experiència en bodes i esdeveniments, tindrà cura de cada detall perquè quedi perfecte i gaudiu amb la vostra gent d'un dia que no oblidareu.";
	$lang["hom_btn_org_boda"]    = "Organitzeu la nostra boda";

	$lang["hom_fotos_h3"]        = "Ells ja l'han fet realitat!";
	$lang["hom_fotos_txt"]       = "Aquestes són algunes de les parelles que ens han permès formar part del seu gran dia.<br> Visiteu la nostra galeria i descobriu-ne la resta!";
	$lang["hom_btn_ver_fotos"]   = "Veure galeria";

	$lang["hom_puntos_h3"]       = "Comenceu a fer realitat el vostre somni!";
	$lang["hom_puntos_txt"]      = "Us oferim els millors serveis perquè viviu el millor dia de la vostra vida.";
	$lang["hom_punto_1"]         = "Una meravellosa cala on dir-vos SÍ, VULL davant del mar.";
	$lang["hom_punto_2"]         = "L'aperitiu de benvinguda més temptador, envoltat de jardins i piscines, amb la mar sempre present.";
	$lang["hom_punto_3"]         = "El luxe de poder allotjar les persones que més estimeu en un hotel 4*.";
	$lang["hom_punto_4"]         = "Tenim una llarga tradició gastronòmica: estem especialitzats en plats de la zona que adaptarem a les vostres necessitats.";
	$lang["hom_punto_5"]         = "La millor cerimònia nupcial amb validesa jurídica.";
	$lang["hom_punto_6"]         = "Sala de banquets de vidre amb vistes a la platja i capacitat per a 250 convidats.";
	$lang["hom_punto_7"]         = "Una meravellosa Suite Nupcial, la vostra fantasia feta realitat.";
	$lang["hom_punto_8"]         = "A la vostra disposició un dels equips més professionals i experimentats en l'organització de bodes.";
	$lang["hom_punto_9"]         = "Les millors instal·lacions al vostre servei, una cala enfront del mar, jardins, sala de banquets, discoteca, zona d'esbarjo infantil, allotjament per a tots els convidats i pàrquing.";

	$lang["hom_promos_h3"]       = "Promocions";
	$lang["hom_promos_txt"]      = "Sabem que un esdeveniment d'aquesta magnitud pot comportar un esforç que va més enllà del normal. Per això, a l'Hotel Ametlla Mar 4* treballem per oferir <strong>el millor servei al millor preu</strong>, perquè pugueu celebrar el vostre gran dia sense preocupar-vos de la butxaca.";
	$lang["hom_btn_ver_promos"]  = "Veure totes";

	$lang["hom_opi_h3"]          = "Missatges que ens enamoren...";
	$lang["hom_opi_1"]           = "Casar-se en plena platja és un marc incomparable. L'organització un deu, van estar pendents tant abans de l'esdeveniment, ajudant-nos a organitzar la boda perfecta, com després, que van aconseguir que fos una nit molt molt especial. Us ho recomano i podeu estar tranquil·les, que tindreu una boda espectacular.";
	$lang["hom_opi_2"]           = "Excel·lents professionals, molt atents tant en el tracte previ a la boda com el dia de la boda i després de la mateixa. En tot moment han estat pendents de les nostres peticions, el menú té una gran quantitat d'entrants amb els quals quasi tots els convidats van quedar satisfets, després el sopar tot molt bo. El tracte dels cambrers també va ser molt bo.";
	$lang["hom_opi_3"]           = "Gràcies a Sara i Tanit per haver-nos organitzat el dia més feliç de la nostra vida. Realment ens van ajudar i ens van facilitar tot el que vam necessitar i més. Tot va ser de luxe Immillorable! Ho recomanem 1000%. I una de les opcions que més ens va convèncer és que vam poder allotjar a tots els nostres convidats. Les habitacions genial i a nosaltres ens van regalar la Suite Nupcial Perfecta!";
	$lang["hom_btn_ver_opi"]     = "Llegir més";

    $lang[""]	= "";
    
?>