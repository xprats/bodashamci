<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			       = "Cerimònies a la platja - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/casament-a-la-platja";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."boda-en-la-playa";

	$lang["meta_description"]      = "L'espai perfecte per a celebrar un casament civil a la platja. A l'Hotel Ametlla Mar 4* podreu celebrar una cerimònia civil a la platja amb validesa jurídica.";

	$lang["cere_h1"]			   = "Cerimònia a la platja Hotel Ametlla Mar 4*";
	$lang["cere_h2"]			   = "A l'Hotel Ametlla Mar 4* fem realitat els vostres somnis en un espai perfecte";
	$lang["cere_h3"]			   = "Caseu-vos a la platja!";
	$lang["cere_subtext"]		   = "<p>Si heu somiat celebrar el vostre <strong>casament a la platja</strong>, davant el mar, l'<strong>Hotel Ametlla Mar 4*</strong> és sense cap dubte l'espai perfecte.</p>
									<p>Ho farem realitat posant a la vostra disposició la nostra àmplia experiència organitzant <strong>ceremònies civils amb validesa jurídica</strong>.</p>";
	$lang["cere_mas_info"]	       = "Més informació";
	$lang["cere_menus"]		       = "Descarregar menús";

	$lang["cere_playa_h4"]	       = "Cerimònia a la platja amb validesa jurídica";
	$lang["cere_playa_desc"]	   = "<strong>Us podeu casar de manera legal directament en la cerimònia a la platja</strong>, i convertir l'acte en un  moment molt especial.";
	$lang["cere_playa_desc2"]	   = "Per fer-ho, heu de sol·licitar al vostre ajuntament el trasllat de l'expedient matrimonial.";

	$lang["cere_bodas_h4"]	       = "Bodes originals i úniques a la platja";
	$lang["cere_bodas_desc"]	   = "El dia de la boda <strong>només haureu de viure les emocions</strong>.";
	$lang["cere_bodas_desc2"]	   = "Entre tots <strong>farem possible el vostre gran dia:</strong> aquest serà el nostre regal.";
	$lang["cere_galeria"]	       = "Veure galeria";

	$lang["cere_personaliza_h4"]   = "Personalitzeu la vostra boda";
	$lang["cere_personaliza_desc"] = "Cada boda és diferent perquè no hi ha dues parelles iguals. <strong>Si ho heu somiat, podem organitzar-ho</strong>.";
	
	$lang["cere_galeria_h3"]	   = "Ho tenim tot a punt!";
	$lang["cere_galeria_desc"]	   = "Totes les bodes comencen amb una història d'amor.<br /> Les fotos que veieu no són muntatges; són històries reals de parelles que s'han casat al nostre hotel.";
	
	$lang[""]	= "";
    
?>