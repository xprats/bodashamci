<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			        = "Avís legal - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/nota-legal";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."nota-legal";

	$lang["meta_description"]       = "Avís legal. Ens prenem molt seriosament la seguretat de tots els nostres clients, i les seves dades personals no son una excepció.";

	$lang["avi_h1"]			        = "Nota Legal";

	$lang["avi_txt_1"]		        = "En compliment del que es preveu en l'artícle 10 de la Llei 34/2002, d'11 de juliol, de Serveis de la Societat de la Informació i de Comerç Electrònic, s'informa que <a href=\"http://www.hotelametllamar.es\ca\" target=\"_blank\">www.hotelametllamar.es</a> és un domini de l'empresa: <strong>RV Hotels Turistics, S.L.U.</strong>, amb domicili social C/. Diputació, 238. Entl. 3a, Barcelona (08007) amb telèfon <a href=\"tel:+34935036039\">(+34) 935 036 039</a>, fax (+34) 935 036 044 i correu electrònic <a href=\"mailto:info@rvhotels.es\">info@rvhotels.es</a>.";
	$lang["avi_txt_2"]		        = "La societat esmentada més amunt està registrada com:";
	$lang["avi_txt_3"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> amb CIF: B82573338. Figura inscrita en el Registre Mercantil de Barcelona, tom 35364, foli 34, full 265150, inscripció 3a.";
	$lang["avi_txt_4"]		        = "<strong>Número de registre turístic:</strong> HTE-000826.";

    $lang[""]	= "";
?>