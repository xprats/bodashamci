<?php
	
	/* estamos en idioma -- CA -- */

	$lang["n_opiniones"] = 4;

	/* textos para la visualizacion del listado de mantenimientos */
	$lang["titulo"]			       = "Opinions de parelles - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/opinions-de-parelles";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."opiniones-de-parejas";

	$lang["meta_description"]      = "Aquestes son algunes opinions reals de parelles que ja han fet realitat el seu somni i han celebrat el seu casament a la platja a l'Hotel Ametlla Mar 4* Deixa't inspirar!";

	$lang["o_h1"]	               = "Opinions de bodes al'Hotel Ametlla Mar 4*";
	$lang["o_h2"]	               = "Conteu-nos la vostra experiència i compartiu amb nosaltres com va ser el vostre gran dia";

	/* opiniones de nuestros clientes */
	$lang["o_t1"]	               = "Hem sortit molt contents, el tracte amb Carla i Tanit ha sigut immillorable, molt atentes, molt agradables, molt simpàtiques amb nosaltres tant en la preparació com en el dia del casament, en el dia del casament molta organització, tot va sortir perfecte. La Suite Nupcial em va deixar enamorada de bonita que és i les precioses vistes que té a la platja, des de la terrassa veia mentre em vestia com muntaven el lloc de la cerimònia, és un moment preciós. El menjar molt bé i el servei de cambrers aquell dia va estar de 10. Sens dubte no ens equivoquem en aquesta elecció i a més a més la família va quedar encantada i tots van opinar que va ser una cerimònia i un casament preciós...";
	$lang["o_f1"]	               = "Nerea & Josep Ramon<br>25 de Maig de 2018";

	$lang["o_t2"]	               = "Casar-se en plena platja és un marc incomparable. L'organització un deu, van estar pendents tant abans de l'esdeveniment, ajudant-nos a organitzar el casament perfecte, com després, que van aconseguir que fos una nit molt molt especial. Us ho recomano i podeu estar tranquil·les, que tindreu un casament espectacular.";
	$lang["o_f2"]	               = "Susana & Abraham<br>3 de Juny de 2017";

	$lang["o_t3"]	               = "Excel·lents professionals, molt atents tant en el tracte previ al casament com el dia del casament i després del mateix. En tot moment han estat pendents de les nostres peticions, el menú té una gran quantitat d'entrants amb els quals quasi tots els convidats van quedar satisfets, després el sopar tot molt bo. El tracte dels cambrers també va ser molt bo.";
	$lang["o_f3"]	               = "Chusa & Nico<br>22 d'Abril de 2017";

	$lang["o_t4"]	               = "Gràcies a Sara i Tanit per haver-nos organitzat el dia més feliç de la nostra vida. Realment ens van ajudar i ens van facilitar tot el que necessitem i més. Tot va ser de luxe Immillorable! Ho recomanem 1000%. I una de les opcions que més ens va convèncer és que vam poder allotjar a tots els nostres convidats. Les habitacions genial i a nosaltres ens van regalar la Suite Nupcial Perfecta!";
	$lang["o_f4"]	               = "Claudia & Juan Pedro<br>21 d'Octubre de 2016";
	/* / opiniones de nuestros clientes */

	$lang[""]	= "";
?>