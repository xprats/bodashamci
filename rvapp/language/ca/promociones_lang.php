<?php
	
	/* estamos en idioma -- CA -- */

	$lang["titulo"]			         = "Promocions i ofertes - Casaments a la platja - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."ca/promocions";
	$lang["hreflang_codigo"]  = "es";
	$lang["hreflang_url"]     = base_url()."promociones";

	$lang["meta_description"]        = "Promocions per a casaments a la platja. Us convidem a passar un dia a l'Hotel Ametlla Mar 4* i escollir l'oferta que millor s'adapti a vosaltres.";

    $lang["promociones_h1"]		     = "Promocions de bodes a la platja";
	$lang["promociones_h2"]		     = "Us convidem a passar un dia a l'Hotel Ametlla Mar 4* i escollir l'oferta que millor s'adapti a vosaltres";
	$lang["promociones_h3"]		     = "Promocions";
	
	/* PROMOCION 1: prefijo - "promo1_" */
	$lang["promo1_canonical"]		 = base_url()."ca/allotjament-gratuit-convidats";
	$lang["promo1_hreflang_url"]     = base_url()."alojamiento-gratuito-invitados";
	$lang["promo1_meta_description"] = "Amb aquesta promoció tots els vostres convidats es podran quedar gratis a dormir en un fantàstic hotel 4 estrelles, allargant el casament durant tot el cap de setmana.";
	$lang["promo1_url"]		         = "allotjament-gratuit-convidats";
	$lang["promo1_titulo"]	         = "Habitacions per a tots els convidats inclosos en el preu";
	$lang["promo1_texto"]	         = "L'Hotel Ametlla Mar 4* ofereix un promoció increïble perquè viviu un esdeveniment inoblidable. Podreu <strong>allotjar gratuïtament tots els vostres convidats</strong> en un fantàstic hotel  de 4 estrelles i allargar així la celebració durant tot el cap de setmana.";
	$lang["promo1_boton"]	         = "Més informació";
		
	$lang["promo1_link_menu"]        = '<strong>El nostre fantàstic <a href="'.base_url().'menus/menu_la_cala_CA.pdf" target="_blank">Menú La Cala</a> inclou:</strong>';
	$lang["promo1_lista"] 	         = '<li><i class="far fa-heart"></i> Cerimònia civil a la platja amb possibilitat de validesa jurídica.
							   		    <li><i class="far fa-heart"></i> Aperitiu als jardins amb més de 30 propostes fredes i calentes.
							   		    <li><i class="far fa-heart"></i> Banquet amb gran varietat de plats a triar.
							   		    <li><i class="far fa-heart"></i> Pastís Nupcial.
							   		    <li><i class="far fa-heart"></i> Discoteca privada.
							   		    <li><i class="far fa-heart"></i> Allotjament en l\'exclusiva Suite Nupcial.
							   		    <li><i class="far fa-heart"></i> Allotjament gratuït per a tots els convidats.';
	$lang["promo1_condiciones"]      = "<strong>Condicions:</strong> oferta per a bodes celebrades divendres i diumenge.";
	$lang["promo1_mas_info"] 	     = "Més informació";
	$lang["promo1_boton_menu"]	     = '<a href="'.base_url().'menus/menu_la_cala_CA.pdf" target="_blank" class="btn btn-default">Descarregar menú</a>';
	/* / PROMOCION 1 */

	/* PROMOCION 2: prefijo - "promo2_" */
	$lang["promo2_canonical"]		 = base_url()."ca/promocio-casaments-dissabte";
	$lang["promo2_hreflang_url"]     = base_url()."promocion-bodas-sabados";
	$lang["promo2_meta_description"] = "Casa't un dissabte a l'Hotel Ametlla Mar 4* i podràs aprofitar aquesta fantàstica promoció. La boda a la platja que sempre havies somniat per un preu inigualable.";
	$lang["promo2_url"]		         = "promocio-casaments-dissabte";
	$lang["promo2_titulo"]	         = "La boda que sempre heu somiat per només 95 €";
	$lang["promo2_texto"]	         = "L'Hotel Ametlla Mar 4* ofereix una proposta increïble perquè viviu un esdeveniment inoblidable. Perquè podem i volem fer realitat tots els vostres somnis.";
	$lang["promo2_boton"]	         = "Més informació";
		
	$lang["promo2_link_menu"]        = '<strong>El nostre fantàstic <a href="'.base_url().'menus/menu_aguamarina_CA.pdf" target="_blank">Menú Aguamarina</a> inclou:</strong>';
	$lang["promo2_lista"] 	         = '<li><i class="far fa-heart"></i>Cerimònia civil a la platja amb possibilitat de validesa jurídica.
								        <li><i class="far fa-heart"></i>Gran aperitiu amb 30 propostes diferents + 2 bufets + show cooking + pernil i 3 hores de barra lliure.
									    <li><i class="far fa-heart"></i>Banquet amb entrant, sorbet, plat principal, postres a escollir entre gran varietat de propostes i Pastís Nupcial.
								        <li><i class="far fa-heart"></i>Cafès, pastes de té i carro de licors.
								        <li><i class="far fa-heart"></i>Suite Nupcial per a la nit de noces en un magnífic hotel de 4 estrelles amb vistes al mar.
								        <li><i class="far fa-heart"></i>Equip humà altament dedicat i professional que tindrà cura de tots els detalls.
								        <li><i class="far fa-heart"></i>Servei de wedding planner.
								        <li><i class="far fa-heart"></i>Muntatge de taules i decoració.
								        <li><i class="far fa-heart"></i>Prova de menú gratuïta per a 4 persones.
								        <li><i class="far fa-heart"></i>Minutes personalitzades i cobert.
								        <li><i class="far fa-heart"></i>Especial Pack barra lliure.';
	$lang["promo2_condiciones"] 	  = "<strong>Condicions:</strong> oferta per a bodes celebrades en dissabte.";
	$lang["promo2_mas_info"] 		  = "Més informació";
	$lang["promo2_boton_menu"]		  = '<a href="'.base_url().'menus/menu_aguamarina_CA.pdf" target="_blank" class="btn btn-default">Descarregar menú</a>';
	/* / PROMOCION 2 */

	/* PROMOCION 3: prefijo - "promo3_" */
	$lang["promo3_canonical"]		 = base_url()."ca/promocio-casat-juny";
	$lang["promo3_hreflang_url"]     = base_url()."promocion-casate-junio";
	$lang["promo3_meta_description"] = "Casa't al juny de 2019 por 80€ tot inclòs";
	$lang["promo3_url"]		         = "promocio-casat-juny";
	$lang["promo3_titulo"]	         = "Casa't al juny de 2019 por 80€ tot inclòs";
	$lang["promo3_texto"]	         = "L'Hotel Ametlla Mar 4* ofereix una proposta increïble perquè viviu un esdeveniment inoblidable. Perquè podem i volem fer realitat tots els vostres somnis.";
	$lang["promo3_boton"]	         = "Més informació";
		
	$lang["promo3_link_menu"]        = '<strong>Nuestro fantástico <a href="'.base_url().'menus/menu_aguamarina_CA.pdf" target="_blank">Menú Aguamarina</a> incluye:</strong>';
	$lang["promo3_lista"] 	         = '<li><i class="far fa-heart"></i>Cerimònia civil a la platja.
										<li><i class="far fa-heart"></i>Gran variedad de aperitivos calientes y fríos.
										<li><i class="far fa-heart"></i>Banquet amb plat principal, segon, postres, pastís nupcial i celler.
										<li><i class="far fa-heart"></i>Suite Nupcial per a la nit de noces en un magnífic hotel de 4 estrelles amb vistes al mar.
										<li><i class="far fa-heart"></i>2 hores de barra lliure.';
	$lang["promo3_condiciones"]      = "<strong>Condicions:</strong> oferta per a bodas celebrades al juny.";
	$lang["promo3_mas_info"]         = "Més informació";
	$lang["promo3_boton_menu"]       = '<a href="'.base_url().'menus/menu_aguamarina_CA.pdf" target="_blank" class="btn btn-default">Descarregar menú</a>';
	/* / PROMOCION 3 */

	$lang[""]	= "";
?>