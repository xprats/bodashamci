<?php
	
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			         = "Promociones y ofertas para bodas en la playa - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."promociones";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/promocions";

	$lang["meta_description"]        = "Promociones de bodas en la playa. Os invitamos a pasar un día en el Hotel Ametlla Mar 4* y escoger la oferta que mejor se adapte a vosotros.";
	
    $lang["promociones_h1"]		     = "Promociones de bodas en la playa";
	$lang["promociones_h2"]		     = "Os invitamos a pasar un día en el Hotel Ametlla Mar 4* y escoger la oferta que mejor se adapte a vosotros";
	$lang["promociones_h3"]		     = "Promociones";
	
	/* PROMOCION 1: prefijo - "promo1_" */
	$lang["promo1_canonical"]		 = base_url()."alojamiento-gratuito-invitados";
	$lang["promo1_hreflang_url"]     = base_url()."ca/allotjament-gratuit-convidats";
	$lang["promo1_meta_description"] = "Con esta promoción podréis alojar gratuitamente a todos vuestros invitados en un fantástico hotel 4 estrellas, alargando así vuestra boda en la playa durante todo el fin de semana.";
	$lang["promo1_url"]		         = "alojamiento-gratuito-invitados";
	$lang["promo1_titulo"]	         = "Habitaciones para todos vuestros invitados incluidas en el precio";
	$lang["promo1_texto"]	         = "El Hotel Ametlla Mar 4* ofrece una promoción increíble para que viváis un evento inolvidable. Podréis <strong>alojar gratuitamente a todos vuestros invitados</strong> en un fantástico hotel de 4 estrellas, alargando así vuestra celebración a todo el fin de semana.";
	$lang["promo1_boton"]	         = "Más información";
		
	$lang["promo1_link_menu"]        = '<strong>Nuestro fantástico <a href="'.base_url().'menus/menu_la_cala_ES.pdf" target="_blank">Menú La Cala</a> incluye:</strong>';
	$lang["promo1_lista"] 	         = '<li><i class="far fa-heart"></i> Ceremonia civil en la playa con posibilidad de validez jurídica.
								        <li><i class="far fa-heart"></i> Aperitivo en los jardines con más de 30 propuestas frías y calientes.
								        <li><i class="far fa-heart"></i> Banquete con gran variedad de platos a elegir.
								        <li><i class="far fa-heart"></i> Pastel Nupcial.
								        <li><i class="far fa-heart"></i> Discoteca privada.
								        <li><i class="far fa-heart"></i> Alojamiento en la exclusiva Suite Nupcial.
								        <li><i class="far fa-heart"></i> Alojamiento gratuito para todos los invitados.';
	$lang["promo1_condiciones"]      = "<strong>Condiciones:</strong> oferta para bodas en viernes y domingo.";
	$lang["promo1_mas_info"]         = "Más información";
	$lang["promo1_boton_menu"]       = '<a href="'.base_url().'menus/menu_la_cala_ES.pdf" target="_blank" class="btn btn-default">Descargar menú</a>';
	/* / PROMOCION 1 */

	/* PROMOCION 2: prefijo - "promo2_" */
	$lang["promo2_canonical"]		 = base_url()."promocion-bodas-sabados";
	$lang["promo2_hreflang_url"]     = base_url()."ca/promocio-casaments-dissabte";
	$lang["promo2_meta_description"] = "Cásate un sábado en el Hotel Ametlla Mar 4*y podrás aprovechar nuestra fantástica promoción. La boda en la playa que siempre has soñado a un precio inigualable.";
	$lang["promo2_url"]		         = "promocion-bodas-sabados";
	$lang["promo2_titulo"]	         = "La boda que siempre has soñado por sólo 95 €";
	$lang["promo2_texto"]	         = "El Hotel Ametlla Mar 4* ofrece una propuesta increíble para que viváis un evento inolvidable. Porque podemos y queremos hacer realidad todos vuestros sueños.";
	$lang["promo2_boton"]	         = "Más información";
		
	$lang["promo2_link_menu"]        = '<strong>Nuestro fantástico <a href="'.base_url().'menus/menu_aguamarina_ES.pdf" target="_blank">Menú Aguamarina</a> incluye:</strong>';
	$lang["promo2_lista"] 	         = '<li><i class="far fa-heart"></i>Ceremonia civil en la playa con posibilidad de validez jurídica.
								        <li><i class="far fa-heart"></i>Gran aperitivo con 30 propuestas diferentes + 2 buffets + show cooking + jamón y 3 horas de barra libre.
								        <li><i class="far fa-heart"></i>Banquete con entrante, sorbete, plato principal, postres a escoger entre gran variedad de propuestas y Pastel Nupcial.
								        <li><i class="far fa-heart"></i>Cafés, petits fours y carro de licores.
								        <li><i class="far fa-heart"></i>Suite Nupcial para la noche de bodas en un magnífico hotel de 4 estrellas con vistas al mar.
									    <li><i class="far fa-heart"></i>Equipo humano altamente dedicado y profesional que mimará hasta el último detalle.
									    <li><i class="far fa-heart"></i>Servicio de wedding planner.
									    <li><i class="far fa-heart"></i>Montaje de mesas y decoración.
									    <li><i class="far fa-heart"></i>Prueba de menú gratuita para 4 personas.
									    <li><i class="far fa-heart"></i>Minutas personalizadas y sitting.
									    <li><i class="far fa-heart"></i>Especial Pack barra libre.';
	$lang["promo2_condiciones"]      = "<strong>Condiciones:</strong> oferta para bodas celebradas en sábado.";
	$lang["promo2_mas_info"]         = "Más información";
	$lang["promo2_boton_menu"]       = '<a href="'.base_url().'menus/menu_aguamarina_ES.pdf" target="_blank" class="btn btn-default">Descargar menú</a>';
	/* / PROMOCION 2 */

	/* PROMOCION 3: prefijo - "promo3_" */
	$lang["promo3_canonical"]		 = base_url()."promocion-casate-junio";
	$lang["promo3_hreflang_url"]     = base_url()."ca/promocio-casat-juny";
	$lang["promo3_meta_description"] = "Cásate en junio de 2019 por 80€ todo incluido";
	$lang["promo3_url"]		         = "promocion-casate-junio";
	$lang["promo3_titulo"]	         = "Cásate en junio de 2019 por 80€ todo incluido";
	$lang["promo3_texto"]	         = "El Hotel Ametlla Mar 4* ofrece una propuesta increíble para que viváis un evento inolvidable. Porque podemos y queremos hacer realidad todos vuestros sueños.";
	$lang["promo3_boton"]	         = "Más información";
		
	$lang["promo3_link_menu"]        = '<strong>Nuestro fantástico <a href="'.base_url().'menus/menu_aguamarina_ES.pdf" target="_blank">Menú Aguamarina</a> incluye:</strong>';
	$lang["promo3_lista"] 	         = '<li><i class="far fa-heart"></i>Ceremonia civil en la playa.
								        <li><i class="far fa-heart"></i>Gran variedad de aperitivos calientes y fríos.
								        <li><i class="far fa-heart"></i>Banquete con plato principal, segundo, postre, pastel nupcial y bodega.
								        <li><i class="far fa-heart"></i>Suite Nupcial para la noche de bodas en un magnífico hotel de 4 estrellas con vistas al mar.
									    <li><i class="far fa-heart"></i>2 horas de barra libre.';
	$lang["promo3_condiciones"]      = "<strong>Condiciones:</strong> oferta para bodas celebradas en junio.";
	$lang["promo3_mas_info"]         = "Más información";
	$lang["promo3_boton_menu"]       = '<a href="'.base_url().'menus/menu_aguamarina_ES.pdf" target="_blank" class="btn btn-default">Descargar menú</a>';
	/* / PROMOCION 3 */

	$lang[""]	= "";
?>