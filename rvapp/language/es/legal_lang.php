<?php
	
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			        = "Aviso legal - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."nota-legal";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/nota-legal";

	$lang["meta_description"]       = "Aviso legal. Nos tomamos muy en serio la seguridad de todos nuestros clientes, y sus datos personales no son una excepción.";

	$lang["avi_h1"]			        = "Nota Legal";

	$lang["avi_txt_1"]		        = "En cumplimiento de lo que prevé el artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico, se informa que <a href=\"http://www.hotelametllamar.es\ca\" target=\"_blank\">www.hotelametllamar.es</a> es un dominio de la siguiente empresa: <strong>RV Hotels Turistics, S.L.U.</strong>, con domicilio social C/. Diputació, 238. Entr 3ª, Barcelona (08007) con teléfono <a href=\"tel:+34935036039\">(+34) 935 036 039</a> y fax (+34) 935 036 044 y correo electrónico <a href=\"mailto:info@rvhotels.es\">info@rvhotels.es</a>.";
	$lang["avi_txt_2"]		        = "La sociedad arriba nombrada está registrada como sigue:";
	$lang["avi_txt_3"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> con el CIF: B82573338. Figura inscrita en el Registro Mercantil de Barcelona, tomo 35364, folio 34, hoja 265150, inscripción 3ª.";
	$lang["avi_txt_4"]		        = "<strong>Número de registro turístico:</strong> HTE-000826.";

    $lang[""]	= "";
?>