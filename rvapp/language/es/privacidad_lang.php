<?php
	
	/* estamos en idioma -- ES -- */
	
	$lang["titulo"]			        = "Política de privacidad - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."politica-de-privacidad";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/politica-de-privacitat";

	$lang["meta_description"]       = "Política de Privacidad. Nos tomamos muy en serio la seguridad de todos nuestros clientes, y sus datos personales no son una excepción.";

	$lang["pri_h1"]			        = "Política de Privacidad";
	
	$lang["pri_txt_1"]		        = "En RV Hotels nos tomamos muy en serio la seguridad de todos nuestros clientes, y sus datos personales no son una excepción.";
	$lang["pri_txt_2"]				= "<strong>RV Hotels Turistics, S.L.U.</strong> protege los datos personales según lo establecido en el Reglamento (UE) 2016/679 del Parlamento y del Consejo, de 27 de abril de 2016, relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos con el objeto que se respeten las libertades y los derechos fundamentales de las personas a quien pertenezcan esos datos.";
	$lang["pri_txt_3"]				= "Sus datos serán tratados con la máxima confidencialidad para gestionar nuestra relación comercial. Los datos se conservaran hasta que se solicite la supresión a la siguiente dirección <a href=\"mailto:lopd@gruprv.es\">lopd@gruprv.es</a>, o lo establezca una disposición legal. Puede presentar una reclamación ante la autoridad de control competente (<a href=\"https://www.agpd.es\" target=\"_blank\">www.agpd.es</a>) si considera que el tratamiento no se ajusta a la normativa vigente.";
	$lang["pri_txt_4"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> está legitimada para tratar sus datos porque son necesarios para prestar el servicio solicitado o por  el consentimiento que nos otorga en la los diferentes formularios de contacto de la web.";
	$lang["pri_txt_5"]		        = "Los datos no se cederán a terceros salvo en los casos en que exista una obligación legal.";
	$lang["pri_txt_6"]		        = "Para ejercer los derechos de acceso, rectificación, supresión y portabilidad de sus datos, y la limitación u oposición a su tratamiento, se debe dirigir por escrito a <a href=\"mailto:lopd@gruprv.es\">lopd@gruprv.es</a>.";
	$lang["pri_txt_7"]		        = "Los Usuarios, mediante la marcación de las casillas correspondientes y entrada de datos en los campos, marcados con un asterisco (*) en el formulario de contacto o presentados en formularios de descarga, aceptan expresamente y de forma libre e inequívoca, que sus datos son necesarios para atender su petición, por parte del prestador, siendo voluntaria la inclusión de datos en los campos restantes. El Usuario garantiza que los datos personales facilitados son veraces y se hace responsable de comunicar cualquier modificación de los mismos.";
	$lang["pri_txt_8"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> está cumpliendo con todas las disposiciones de las normativas  para el tratamiento de los datos personales de su responsabilidad, y con los principios descritos en el artículo 5 del GDPR, por los cuales son tratados de manera lícita, leal y transparente en relación con el interesado y adecuados, pertinentes y limitados a lo necesario en relación con los fines para los que son tratados.";
	$lang["pri_txt_9"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong> garantiza que ha implementado políticas técnicas y organizativas apropiadas para aplicar las medidas de seguridad que establecen el GDPR con el fin de proteger los derechos y libertades de los Usuarios y les ha comunicado la información adecuada para que puedan ejercerlos.";
	$lang["pri_txt_10"]		        = "<strong>Cláusula adicional para la recepción de CV e información sobre puestos de trabajo vacantes.</strong>";
	$lang["pri_txt_11"]		        = "Tratamos la información que nos facilita con el fin de mantenerle informado de las distintas vacantes a un puesto de trabajo que se produzcan en nuestra organización.";
	$lang["pri_txt_12"]		        = "Los datos proporcionados se conservarán hasta la adjudicación de un puesto de trabajo o hasta que usted ejerza su derecho de cancelación y por tanto tiene derecho a acceder a sus datos personales, rectificar los datos inexactos o solicitar su supresión cuando los datos ya no sean necesarios. Los datos no se cederán a terceros.";
	$lang["pri_txt_13"]		        = "Hemos adoptado todas las medidas de seguridad establecidas en la legislación actual sobre protección de datos. Puede ejercer en todo momento sus derechos de acceso, rectificación, supresión y portabilidad de sus datos, y la limitación u oposición a su tratamiento, mediante escrito dirigido a <a href=\"mailto:lopd@gruprv.es\">lopd@gruprv.es</a>. También tiene derecho a presentar una reclamación ante la autoridad de control competente (<a href=\"https://www.agpd.es\" target=\"_blank\">www.agpd.es</a>) si considera que el tratamiento no se ajusta a la normativa vigente.";

	$lang["pri_txt_14"]		        = "Dirección";
	$lang["pri_txt_15"]		        = "Teléfono";

	$lang["pri_txt_16"]		        = "<strong>RV Hotels Turistics, S.L.U.</strong>, a través de su web, utilizará cookies cuando el Usuario navegue por la website de <a href=\"http://www.hotelametllamar.com\" target=\"_blank\">www.hotelametllamar.com</a>. Las cookies se asocian únicamente con un Usuario anónimo y su ordenador y no proporcionan referencias que permitan deducir el nombre y apellidos del Usuario. Gracias a las cookies, resulta posible que reconozca a los Usuarios registrados después de que éstos se hayan registrado por primera vez, sin que tengan que registrarse en cada visita para acceder a las áreas y servicios reservados exclusivamente a ellos. El Usuario tiene la opción de impedir la generación de cookies, mediante la selección de la correspondiente opción en su programa navegador.";

	$lang["pri_txt_17"]		        = "Cualquier usuario registrado puede en cualquier momento ejercer el derecho a acceder, rectificar y, en su caso, cancelar sus datos de carácter personal suministrados en la web <a href=\"http://www.hotelametllamar.com\" target=\"_blank\">www.hotelametllamar.com</a>, mediante comunicación escrita dirigida a <strong>RV Hotels Turistics, S.L.U.</strong>, C/. Diputació, 238. Entr 3ª, Barcelona (08007), esta comunicación también podrá ser realizada mediante el envío de un correo electrónico a la dirección: <a href=\"mailto:info@rvhotels.es\">info@rvhotels.es</a>.";

    $lang[""]	= "";
?>