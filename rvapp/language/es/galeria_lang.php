<?php
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			       = "Galería - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."galeria";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/galeria";

	
	$lang["meta_description"]      = "Fotos de bodas en la playa. Disfruta de la galería de imágenes de parejas reales que han celebrado su boda en la playa en el Hotel Ametlla Mar 4*. Ellos ya lo han hecho realidad.";

	$lang["g_h3"]		           = "¡Ellos ya lo han hecho realidad!";
    $lang["g_parrafo"]	           = "<p>Todas las fotos que aparecen en esta web corresponden a <strong>parejas reales que han celebrado su boda en el Hotel Ametlla Mar 4*</strong>. Exponer fotos reales nos parece que es <strong>la mejor manera de explicar nuestro estilo</strong>, de cómo organizamos y entendemos las bodas.</p>
							<p><strong>Muchísimas gracias a todas las parejas que habéis confiado en nosotros y que juntos hemos construido vuestro sueño</strong>.</p>";

	$lang["g_cat_todas"]		   = "Todas";
	$lang["g_cat_ceremonia"]	   = "Ceremonia";
	$lang["g_cat_hotel"]		   = "Hotel";
	$lang["g_cat_salones"]		   = "Salones";
	$lang["g_cat_gastronomia"]	   = "Gastronomía";
	$lang["g_cat_novios"]		   = "Novios";
	$lang["g_cat_detalles"]		   = "Detalles";

    $lang[""]	= "";
    
?>