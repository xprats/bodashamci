<?php
	
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			     = "Especialistas en bodas en la playa - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url();
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca";

	$lang["meta_description"]    = "Celebra la boda en la playa que siempre habías soñado en el Hotel Ametlla Mar 4*, en la Costa Dorada. Una boda única en primera línea de mar y con un entorno inigualable.";

	$lang["hom_h1"]			     = "Bodas en la playa Hotel Ametlla Mar 4*";
	$lang["hom_h2"]			     = "Haced vuestro sueño realidad. ¡Casaros en la playa!";
	$lang["hom_h3"]			     = "¡Lo tenemos todo listo!";

	$lang["hom_subtext"]	     = "Si habéis soñado alguna vez celebrar <strong>vuestra boda en la playa</strong>, frente al mar, el Hotel Ametlla Mar 4* es <strong>el espacio perfecto para hacer realidad vuestro sueño</strong>.";

	$lang["hom_btn_casar_playa"] = "¡Queremos casarnos en la playa!";

	$lang["hom_tit_box1"]	     = "Ceremonia civil en la playa";
	$lang["hom_txt_box1"]	     = "Casaros en la playa, en un <strong>entorno único y privilegiado</strong> frente al mar.";
	$lang["hom_tit_box2"]	     = "Personalizad vuestra boda";
	$lang["hom_txt_box2"]	     = "Cada boda es diferente porque no hay dos parejas iguales. <strong>Si lo habéis soñado, podemos organizarlo.</strong>";
	$lang["hom_tit_box3"]	     = "Alojamiento para tus invitados";
	$lang["hom_txt_box3"]	     = "Convertir vuestra boda en una fiesta de todo un fin de semana en nuestro <strong>hotel de 4 estrellas.</strong>";
	$lang["hom_tit_box4"]	     = "La más selecta gastronomía";
	$lang["hom_txt_box4"]	     = "De la mano del Chef Victor Buera, con <strong>productos de primera calidad y de nuestra zona</strong>, les Terres de l'Ebre.";

	$lang["hom_mas_info"]	     = "Más información";

	$lang["hom_txt_blue_box"]    = "Un gran equipo de profesionales con amplia experiencia en bodas y eventos cuidará al máximo de cada detalle para que todo este perfecto y disfrutéis de un día inolvidable junto a los vuestros.";
	$lang["hom_btn_org_boda"]    = "Organizar nuestra boda";

	$lang["hom_fotos_h3"]        = "¡Ellos ya lo han hecho realidad!";
	$lang["hom_fotos_txt"]       = "Algunas de las parejas que nos han permitido formar parte de su gran día.<br /> ¡Visitad nuestra galería para descubrir el resto!";
	$lang["hom_btn_ver_fotos"]   = "Ver galería";

	$lang["hom_puntos_h3"]       = "¡Empiezad a cumplir vuestro sueño!";
	$lang["hom_puntos_txt"]      = "Os ofrecemos los mejores servicios para que viváis el mejor día de vuestra vida.";
	$lang["hom_punto_1"]         = "Una maravillosa cala donde dar el SÍ, QUIERO frente al mar.";
	$lang["hom_punto_2"]         = "El más apetitoso aperitivo de bienvenida rodeado de jardines y piscinas con el mar siempre presente.";
	$lang["hom_punto_3"]         = "El lujo de poder alojar a los que más quieres en un hotel 4*.";
	$lang["hom_punto_4"]         = "Tenemos una larga tradición gastronómica, estamos especializados en platos de la zona que adaptaremos a tus necesidades.";
	$lang["hom_punto_5"]         = "La mejor ceremonia nupcial con validez jurídica.";
	$lang["hom_punto_6"]         = "Salón de banquetes de cristal con vistas a la playa y capacidad para 250 invitados.";
	$lang["hom_punto_7"]         = "Una maravillosa Suite Nupcial, vuestra fantasía hecha realidad.";
	$lang["hom_punto_8"]         = "A vuestra disposición uno de los equipos más profesionales y experimentados en la realización de bodas.";
	$lang["hom_punto_9"]         = "Las mejores instalaciones a tu servicio, cala frente al mar, jardines, salón de banquete, discoteca, zona de recreo infantil, alojamiento para todos tus invitados y párquing.";

	$lang["hom_promos_h3"]       = "Promociones";
	$lang["hom_promos_txt"]      = "Sabemos que un evento de tal envergadura puede conllevar un esfuerzo más allá de lo normal. Por eso, en Hotel Ametlla Mar 4* trabajamos para ofrecer <strong>el mejor servicio al mejor precio</strong> para que puedas celebrar tu gran día sin preocuparte por tu bolsillo.";
	$lang["hom_btn_ver_promos"]  = "Ver todas";

	$lang["hom_opi_h3"]          = "Mensajes que nos enamoran...";
	$lang["hom_opi_1"]           = "¡Es un lugar mágico! Las responsables son de lo más amables y, sobre todo, flexibles. Cuando tuvimos algún contratiempo nos ayudaron a solventarlo lo más rápido posible. Para nosotros que queríamos casarnos en la playa, no había un sitio mejor. ¡Lo volvería a repetir una y otra vez!";
	$lang["hom_opi_2"]           = "¡Perfecto todo! Contentísimos del trato de todo el equipo del hotel. La comida, las habitaciones y toda la ayuda que te hacen... Todos los invitados han quedado contentos y nosotros más todavía. Me volvería a casar en el mismo sitio.";
	$lang["hom_opi_3"]           = "Hemos salido muy contentos, el trato con Carla y Tanit ha sido inmejorable, muy atentas, muy agradables, muy simpáticas con nosotros tanto en la preparación como el día de la boda, en el día de la boda mucha organización, todo salió perfecto. La Suite Nupcial me dejó enamorada de lo bonita que es y las preciosas vistas que tiene a la playa, desde la terraza veía mientras me vestía como montaban el sitio de la ceremonia, es un momento precioso. La comida muy bien y el servicio de camareros ese día estuvo de 10. Sin duda no nos equivocamos en esta elección y además la familia quedó encantada y todos opinaron que fue una ceremonia y una boda preciosa...";
	$lang["hom_btn_ver_opi"]     = "Leer más";

    $lang[""]	= "";
    
?>