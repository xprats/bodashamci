<?php
	
	/* estamos en idioma -- ES -- */

	$lang["n_opiniones"] = 6;

	/* textos para la visualizacion del listado de mantenimientos */
	$lang["titulo"]			       = "Opiniones de parejas - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."opiniones-de-parejas";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/opinions-de-parelles";

	$lang["meta_description"]      = "Estas son algunas opiniones reales de parejas que ya han hecho realidad su sueño y han celebrado su boda en la playa en el Hotel Ametlla Mar 4* ¡Déjate inspirar!";

	$lang["o_h1"]	               = "Opiniones de bodas en el Hotel Ametlla Mar 4*";
	$lang["o_h2"]	               = "Contadnos vuestra experiencia y compartid con nosotros como fué vuestro gran día";

	/* opiniones de nuestros clientes */
	$lang["o_t1"]	               = "¡Es un lugar mágico! Las responsables son de lo más amables y, sobre todo, flexibles. Cuando tuvimos algún contratiempo nos ayudaron a solventarlo lo más rápido posible. Para nosotros que queríamos casarnos en la playa, no había un sitio mejor. ¡Lo volvería a repetir una y otra vez!";
	$lang["o_f1"]	               = "Marina y Kiko<br>29 de septiembre de 2018";

	$lang["o_t2"]	               = "¡Perfecto todo! Contentísimos del trato de todo el equipo del hotel. La comida, las habitaciones y toda la ayuda que te hacen... Todos los invitados han quedado contentos y nosotros más todavía. Me volvería a casar en el mismo sitio.";
	$lang["o_f2"]	               = "Saray y Cisco<br>8 de junio de 2018";

	$lang["o_t3"]	               = "Hemos salido muy contentos, el trato con Carla y Tanit ha sido inmejorable, muy atentas, muy agradables, muy simpáticas con nosotros tanto en la preparación como el día de la boda, en el día de la boda mucha organización, todo salió perfecto. La Suite Nupcial me dejó enamorada de lo bonita que es y las preciosas vistas que tiene a la playa, desde la terraza veía mientras me vestía como montaban el sitio de la ceremonia, es un momento precioso. La comida muy bien y el servicio de camareros ese día estuvo de 10. Sin duda no nos equivocamos en esta elección y además la familia quedó encantada y todos opinaron que fue una ceremonia y una boda preciosa...";
	$lang["o_f3"]	               = "Nerea & Josep Ramon<br>25 de mayo de 2018";

	$lang["o_t4"]	               = "Casarse en plena playa es un marco incomparable. La organización un diez, estuvieron pendientes tanto antes del evento, ayudándonos a organizar la boda perfecta, como después, que consiguieron que fuera una noche muy muy especial. Os lo recomiendo y podéis estar tranquilas, que tendréis una boda espectacular.";
	$lang["o_f4"]	               = "Susana & Abraham<br>3 de Junio de 2017";

	$lang["o_t5"]	               = "Excelentes profesionales, muy atentos tanto en el trato previo a la boda como el día de la boda y tras la misma. En todo momento han estado pendientes de nuestras peticiones, el menú tiene una gran cantidad de entrantes con los que casi todos los invitados quedaron satisfechos, después la cena todo muy bueno. El trato de los camareros también fue muy bueno.";
	$lang["o_f5"]	               = "Chusa & Nico<br>22 de Abril de 2017";

	$lang["o_t6"]	               = "Gracias a Sara y Tanit por habernos organizado el día más feliz de nuestra vida. Realmente nos ayudaron y nos facilitaron todo lo que necesitamos y más. Todo fue de lujo ¡Inmejorable! Lo recomendamos 1000%. Y una de las opciones que más nos convenció es que pudimos alojar a todos nuestros invitados. Las habitaciones genial y a nosotros nos regalaron la Suite Nupcial ¡Perfecta!";
	$lang["o_f6"]	               = "Claudia & Juan Pedro<br>21 de Octubre de 2016";
	/* / opiniones de nuestros clientes */

	$lang[""]	= "";
?>