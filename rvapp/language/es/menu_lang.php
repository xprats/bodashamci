<?php
	/*systema de multiidioma en Castellano para Code Igniter*/
	/* sample $lang[] = ""; */

	/* textos para la visualizacion del listado de mantenimientos */
    $lang["m_u_ceremonia"]      = "boda-en-la-playa";
    $lang["m_t_ceremonia"] 	    = "Ceremonia<span>en la playa</span>";

    $lang["m_u_alojamiento"]    = "alojamiento";
    $lang["m_t_alojamiento"]    = "Alojamiento<span>para tus invitados</span>";

    $lang["m_u_gastronomia"] 	= "gastronomia-espacios";
    $lang["m_t_gastronomia"] 	= "Gastronomía<span>espacios</span>";

    $lang["m_u_galeria"] 	    = "galeria";
    $lang["m_t_galeria"] 	    = "Galería<span>de fotos</span>";

    $lang["m_u_promociones"] 	= "promociones";
    $lang["m_t_promociones"] 	= "Promociones<span>ofertas</span>";

    $lang["m_u_opiniones"] 	    = "opiniones-de-parejas";
    $lang["m_t_opiniones"] 	    = "Opiniones<span>de parejas</span>";

    $lang["m_u_preguntas"] 	    = "preguntas-frecuentes";
    $lang["m_t_preguntas"] 	    = "Preguntas<span>frecuentes</span>";

    $lang["m_u_contacto"] 	    = "contacto";
    $lang["m_t_contacto"] 	    = "Información<span>contacto</span>";

    $lang["m_u_contacto_ok"] 	= "gracias-por-contactar-con-nosotros/";

    $lang["m_u_nota_legal"] 	= "nota-legal";
    $lang["m_t_nota_legal"] 	= "Aviso legal";

    $lang["m_u_privacidad"] 	= "politica-de-privacidad";
    $lang["m_t_privacidad"] 	= "Política de privacidad";
    $lang["m_home"] 	        = "";

    $lang[""]	= "";
    
?>