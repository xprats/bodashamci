<?php

	/* estamos en idioma -- ES -- */

	$lang["titulo"]			       = "Menús para bodas en la costa de Tarragona - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."gastronomia-espacios";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/gastronomia-espais";

	$lang["meta_description"]      = "Cásate en la playa y celebra tu banquete en nuestros salones de boda frente al mar. Ponemos a vuestra disposición un entorno privilegiado y una excelente oferta gastronómica.";

	$lang["gas_h1"]			       = "Gastronomía y espacios";
	$lang["gas_h2"]			       = "Grandes salones en el restaurante del hotel con los mejores platos de nuestra tierra";
	$lang["gas_h3"]			       = "El mejor entorno posible";
	$lang["gas_subtext"]	       = "<p>En nuestros salones y jardines disfrutaréis de <strong>un lugar privilegiado para celebrar vuestro enlace</strong>.</p>
									<p>El <strong>Hotel Ametlla Mar 4*</strong> lo pone todo <strong>al servicio de sus novios</strong>.</p>";
	$lang["gas_mas_info"]	       = "Más información";
	$lang["gas_menus"]		       = "Descargar menús";

	$lang["gas_aperitivos_h4"]	   = "Aperitivos";
	$lang["gas_aperitivos_desc"]   = "Disfrutad de un sabroso y extenso aperitivo, rodeados de piscinas y jardines frente al mar.";

	$lang["gas_gastronomia_h4"]	   = "Excelente gastronomía";
	$lang["gas_gastronomia_desc"]  = "Un gran equipo de profesionales con amplia experiencia en banquetes dirigidos por el Chef Victor Buera, están a vuestro servicio para que podáis disfrutar de la más selecta gastronomía basada en productos de primera calidad y naturales de nuestra zona, les Terres de l'Ebre.";
	$lang["gas_gastronomia_desc2"] = "El mejor pescado fresco y las excelentes carnes, frutas y verduras.";

	$lang["gas_banquetes_h4"]	   = "Salón de banquetes";
	$lang["gas_banquetes_desc"]	   = "Un maravilloso salón de cristal y madera con vistas al mar y a los jardines.";
	$lang["gas_banquetes_desc2"]   = "La capacidad máxima del salón es de <strong>350 comensales</strong>.";
	$lang["gas_galeria"]	       = "Ver galería";

	$lang["gas_disco_h4"]	       = "¡Qué no pare la disco!";
	$lang["gas_disco_desc"]	       = "Disponemos de un espacio exclusivo para bailar, en el que podréis alargar vuestra fiesta sin límite horario.";
	$lang["gas_disco_desc2"]	   = "No olvidéis que vuestros invitados pueden quedarse a dormir en el hotel.";
				
	$lang["gas_galeria_h3"]	       = "El espacio ideal para vuestro día";
	$lang["gas_galeria_desc"]	   = "La playa, los jardines, las piscinas, los salones... una combinación ideal para un día inolvidable.";
	/* / textos para la visualizacion del listado de mantenimientos */
	
    $lang[""]	= "";
?>