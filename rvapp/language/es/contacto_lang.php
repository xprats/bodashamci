<?php
	
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			       = " Pide mas informacion sobre tu boda - Hotel Ametlla Mar 4*";

	$lang["canonical"]		  = base_url()."contacto";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/contacte";

	$lang["meta_description"]      = "Cumplid vuestro sueño de casaros en la playa. Contactad con nosotros y haced de vuestra boda en la playa un evento único. ¡Queremos ayudaros a cumplir vuestro sueño!";

	$lang["c_h1"] 	               = "¡Sí, queremos!";
	$lang["c_h2"]	               = "Contactad con nosotros mediante el formulario y os informaremos sin compromiso o si lo preferís, podéis llamarnos directamente.";
	$lang["c_h3"]	               = "¡Queremos ayudaros a cumplir vuestro sueño!";
	$lang["c_nombre"]	           = "Nombre";
	$lang["c_email"]	           = "E-Mail";
	$lang["c_telefono"]	           = "Teléfono";
	$lang["c_comentarios"]	       = "¿En qué podemos ayudaros?";
	// $lang["c_privacidad_1"]	   = "He leído y acepto la <a href=\"".base_url().$lang.lang("m_u_privacidad")."\">política de privacidad</a>";
	$lang["c_privacidad_1"]	       = "He leído y acepto la ";
	$lang["c_privacidad_2"]	       = "política de privacidad";
	/* / textos para la visualizacion del formulario de contacto */

	/* textos para la visualizacion de la confirmacion de envio */ 
	$lang["c_ok_h2"]	           = "<strong>Gracias por escribirnos</strong>,<br /> estamos deseando organizar vuestro gran día.<br /><br />En breve nos pondremos en contacto con vosotros.";
	$lang["c_ok_volver"]           = "Volver al inicio";

	/* textos para la visualizacion ok mailing */ 
	$lang["c_ok_mailing_h1"] 	               = "¡Gracias!";
	$lang["c_ok_mailing_h2"]	           = "Hemos recibido vuestra petición.<br /> <strong>¡Estamos deseando veros en la feria!</strong><br /><br />En breve nos pondremos en contacto con vosotros.";
	/* textos para la visualizacion ok mailing */ 
	$lang["c_adwords_h1"] 	           = "¡Consigue tu entrada!";
	$lang["c_adwords_h2"]	           = "<strong>Contacta con nosotros y obtendrás tu entrada gratuita<br>para la feria Tot Nuvis en Reus del 16 al 18 de noviembre.</strong>";
	/* textos para la visualizacion de la confirmacion de envio */ 

    $lang[""]	= "";
?>