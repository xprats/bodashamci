<?php
	/*systema de multiidioma en Catalan para Code Igniter*/
	/* sample $lang[] = ""; */

	/* textos para la visualizacion del listado de mantenimientos */
    $lang["menus_titulo"]		= "Descubrid nuestros menús";

	/* menu 1: */
		$lang["menu1_titulo"]	= "MENÚ <span class=\"title-clicker\">La Cala</span>";
		$lang["menu1_info1"]	= "<strong>CON ALOJAMIENTO GRATUITO</strong><br>PARA TUS INVITADOS";
		$lang["menu1_info2"]	= "<span style=\"color:#0440AA\"><strong>VIERNES Y DOMINGOS</strong></span>";
		$lang["menu1_desde"]	= "<span class=\"clicker\">desde</span> <span class=\"amarillo\">114€</span>";
		$lang["menu1_link_menu"]= base_url().'menus/menu_la_cala_ES.pdf';
	/* / menu 1 */

	/* menu 2: */
		$lang["menu2_titulo"]	= "MENÚ <span class=\"title-clicker\">Aguamarina</span>";
		$lang["menu2_info1"]	= "<span style=\"color:#00A5DF\"><strong>SÁBADOS</strong></span>";
		$lang["menu2_info2"]	= "<span class=\"clicker todo-inc\">Todo incluido</span>";
		$lang["menu2_desde"]	= "<span class=\"clicker\">desde</span> <span class=\"amarillo\">94€</span>";
		$lang["menu2_link_menu"]= base_url().'menus/menu_aguamarina_ES.pdf';
	/* / menu 2 */

	$lang[""]	= "";
	
?>