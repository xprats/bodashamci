<?php
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			  = "Hotel en la playa para bodas - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."alojamiento";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/allotjament";
	
	$lang["meta_description"] = "Celebra tu ceremonia civil en la playa, con banquete y alojamiento para todos los invitados, todo en el mismo espacio. En el Hotel Ametlla Mar 4* podrás alargar la boda todo el fin de semana.";

	$lang["alo_h1"]			  = "Boda con alojamiento para tus invitados";
	$lang["alo_h2"]			  = "Podrán quedarse a dormir en el Hotel Ametlla Mar 4* y disfrutar hasta el final de la fiesta";
	$lang["alo_h3"]			  = "¡Qué no pare la fiesta!";
	$lang["alo_subtext1"]	  = "<p>En el <strong>Hotel Ametlla Mar 4*</strong>, podréis alojar a todos vuestros invitados, alargando vuestra boda durante todo el fin de semana. </p>
								<p>Que no os preocupe la vuelta a casa: nuestro";
	$lang["alo_subtext_a"]	  = "Menú La Cala";
	$lang["alo_subtext2"]	  = "incluye en el precio <strong>alojamiento para todas las personas que os acompañen</strong>.";
	$lang["alo_mas_info"]	  = "Más información";
	$lang["alo_menus"]		  = "Descargar menús";

	$lang["alo_hotel_h4"]	  = "Un hotel 4* para los invitados";
	$lang["alo_hotel_desc"]	  = "El Hotel Ametlla Mar 4* está situado frente a una maravillosa cala, al sur de la Costa Dorada.";
	$lang["alo_hotel_desc2"]  = "Dispone de las mejores y más completas instalaciones que os permitirán descansar después de la gran fiesta.";

	$lang["alo_suite_h4"]	  = "Nuestro regalo: La Suite Nupcial";
	$lang["alo_suite_desc"]	  = "Podréis disfrutar de vuestra noche de bodas en la inigualable Suite Nupcial con vistas de impresionante belleza. Para sentiros como príncipes de las \"Mil y una noches\".";
	$lang["alo_suite_desc2"]  = "Porque <strong>deseamos que contéis con nosotros</strong> para cumplir vuestros sueños.";
	$lang["alo_galeria"]	  = "Ver galería";

	$lang["alo_galeria_h3"]	  = "Disfrutad de nuestra Suite Nupcial y sentíos como príncipes";
	$lang["alo_galeria_desc"] = "Estas son algunas de las fotos de nuestra Suite y sus espacios.";
	
	$lang[""]	= "";
    
?>