<?php
	
	/* estamos en idioma -- ES -- */

	$lang["titulo"]			      = "Boda en la playa - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."boda-en-la-playa";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/casament-a-la-platja";

	$lang["meta_description"]     = "El espacio perfecto para celebrar una boda civil en la playa. En el Hotel Ametlla Mar 4* podrás celebrar una ceremonia civil en playa con validez jurídica.";

	$lang["cere_h1"]               = "Boda en la playa del Hotel Ametlla Mar 4*";
	$lang["cere_h2"]		       = "En el Hotel Ametlla Mar 4* hacemos realidad vuestros sueños en un espacio perfecto";
	$lang["cere_h3"]		       = "¡Casaros en la playa!";
	$lang["cere_subtext"]	       = "<p>Si habéis soñado celebrar vuestra <strong>boda en la playa</strong>, frente al mar, el <strong>Hotel Ametlla Mar 4*</strong> es sin duda el espacio perfecto.</p>
									<p>Lo haremos realidad poniendo a vuestra disposición nuestra amplia experiencia organizando <strong>ceremonias civiles con validez jurídica</strong>.</p>";
	$lang["cere_mas_info"]	       = "Más información";
	$lang["cere_menus"]		       = "Descargar menús";

	$lang["cere_playa_h4"]	       = "Ceremonia en la playa con validez jurídica";
	$lang["cere_playa_desc"]       = "<strong>Os podéis casar de forma legal directamente en vuestra boda en la playa, haciendo ese momento muy especial.</strong>, haciendo ese momento muy especial.";
	$lang["cere_playa_desc2"]      = "Para ello tenéis que solicitar a vuestro ayuntamiento el traslado del expediente matrimonial.";

	$lang["cere_bodas_h4"]	       = "Bodas originales y únicas en la playa";
	$lang["cere_bodas_desc"]       = "El día de vuestra boda <strong>sólo tendréis que vivir las emociones</strong>.";
	$lang["cere_bodas_desc2"]      = "Entre todos <strong>haremos posible vuestro gran día:</strong> este será nuestro regalo.";
	$lang["cere_galeria"]	       = "Ver galería";

	$lang["cere_personaliza_h4"]   = "Personalizad vuestra boda";
	$lang["cere_personaliza_desc"] = "Cada boda es diferente porque no hay dos parejas iguales. <strong>Si lo habéis soñado, podemos organizarlo</strong>.";
	
	$lang["cere_galeria_h3"]	   = "¡Lo tenemos todo listo!";
	$lang["cere_galeria_desc"]	   = "Todas las bodas empiezan con una historia de amor.<br /> Las fotos que véis no son montajes; son historias reales de parejas que se han casado en nuestro hotel.";
	
	$lang[""]	= "";
    
?>