<?php

	/* estamos en idioma -- ES -- */


	$lang["n_preguntas"] = 14;

	/* textos para la visualizacion del listado de mantenimientos */
	$lang["titulo"]			       = "Preguntas frecuentes - Hotel Ametlla Mar 4*";
	
	$lang["canonical"]		  = base_url()."preguntas-frecuentes";
	$lang["hreflang_codigo"]  = "ca";
	$lang["hreflang_url"]     = base_url()."ca/preguntes-frequents";

	$lang["meta_description"]      = "Al organizar una boda en la playa pueden surgirte muchas dudas. Queremos ayudarte a resolver tus preguntas. Nosotros te lo contamos todo.";

	$lang["p_h1"]				   = "Preguntas frecuentes";
	$lang["p_h2"]				   = "Preparar una boda requiere mucho tiempo y mucha información que alguien os tiene que proporcionar. Nosotros os lo contamos todo.";
	
	$lang["p_p1"]				   = "¿La ceremonia civil es legal?";
	$lang["p_r1"]				   = "Sí, la ceremonia civil en la playa que organizamos en el Hotel Ametlla Mar 4*, puede ser válida jurídicamente. Sólo tenéis que pedir un traslado de expediente a vuestro ayuntamiento.";

	$lang["p_p2"]				   = "¿Cuánto cuesta la ceremonia civil en la playa?";
	$lang["p_r2"]				   = "Si eliges el menú Aguamarina, la ceremonia civil es gratuita. Si escogéis el menú La Cala, la ceremonia civil tiene suplemento.";

	$lang["p_p3"]				   = "¿Podemos decorar la ceremonia civil a nuestro gusto?";
	$lang["p_r3"]				   = "Sí. A partir de nuestra propuesta de decoración, podéis hacer los cambios y personalizar la ceremonia civil en la playa.";

	$lang["p_p4"]				   = "¿Podemos decorar el salón y otros espacios?";
	$lang["p_r4"]				   = "Sí. Podéis cambiar los centros de mesa, la mantelería, decorar la zona de aperitivo con antorchas... es vuestra boda y podéis decorarla como queráis, a vuestro estilo.";

	$lang["p_p5"]				   = "¿El DJ está incluido en el precio?";
	$lang["p_r5"]				   = "Sí. Está incluido el DJ para el aperitivo, banquete y 3 horas de barra libre, en bodas a partir de 95 invitados si escogéis el menú La Cala. Si seleccionáis el menú Aguamarina, el DJ está incluido a partir de 80 comensales.";

	$lang["p_p6"]				   = "¿Podemos elegir la música?";
	$lang["p_r6"]				   = "Por supuesto. La música es muy importante y también habla de vosotros. Haréis reuniones previas a la boda con nuestro DJ, para seleccionar vuestra música.";

	$lang["p_p7"]				   = "¿Ofrecéis servicio de wedding planner?";
	$lang["p_r7"]				   = "Sí. El servicio de wedding planner está incluido en el precio. Ponemos a vuestro servicio un gran equipo de profesionales que os ayudarán a preparar este gran día. Durante vuestra boda estarán pendientes de todo, para que podáis disfrutar de cada momento. No os tendréis que preocupar por nada.";

	$lang["p_p8"]				   = "¿Podemos alargar la barra libre todo lo que deseemos?";
	$lang["p_r8"]				   = "Sí. El hotel dispone de discoteca privada para vosotros sin límite horario.";

	$lang["p_p9"]				   = "¿El IVA está incluido en el precio?";
	$lang["p_r9"]				   = "A todos los precios hay que sumar el 10 % de IVA.";

	$lang["p_p10"]				   = "¿Tenéis servicio de candy bar o recena?";
	$lang["p_r10"]				   = "Sí. Ofrecemos un amplio surtido de propuestas para la recena y servicio de candy bar para endulzar la fiesta a vuestros invitados. El servicio de recena está incluido en el precio en el menú Aguamarina.";

	$lang["p_p11"]				   = "¿Hay servicio de monitor infantil?";
	$lang["p_r11"]				   = "Sí. Una boda es una fiesta para todos. Es muy importante que los niños no se aburran y los padres se relajen y disfruten de la boda. Disponemos de monitores cualificados que cuidarán de los niños durante todo el evento y de magníficas instalaciones infantiles de ocio que estarán a su servicio.";

	$lang["p_p12"]				   = "¿Podemos traer nuestra propia tarta nupcial?";
	$lang["p_r12"]				   = "Sí. Podéis traer vosotros la tarta si así lo deseáis.";

	$lang["p_p13"]				   = "¿Nuestros invitados pueden dormir en el hotel?";
	$lang["p_r13"]				   = "Sí. En el menú La Cala, el alojamiento de los invitados es gratuito. Si escogéis el menú Aguamarina, podéis tener importantes ofertas y promociones para el alojamiento de vuestros invitados.";

	$lang["p_p14"]				   = "¿Hay habitación para novios?";
	$lang["p_r14"]				   = "Sí. Todos nuestros menús incluyen la fantástica Suite para la noche de bodas. Es nuestro regalo para los recién casados.";

    $lang[""]	= "";
?>