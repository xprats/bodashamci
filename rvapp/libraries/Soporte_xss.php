<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Soporte_xss {

    public function __construct() {
    }	
    
    function slugify($text) {
		// vowels
		$pattern = array("'á'", "'ä'", "'à'", "'â'", "'Á'", "'Ä'", "'À'", "'Â'", "'é'", "'ë'", "'è'", "'ê'", "'É'", "'Ë'", "'È'", "'Ê'", "'í'", "'ï'", "'ì'", "'î'", "'Í'", "'Ï'", "'Ì'", "'Î'", "'ó'", "'ö'", "'ò'", "'ô'", "'Ó'", "'Ö'", "'Ò'", "'Ô'", "'ú'", "'ü'", "'ù'", "'û'", "'Ú'", "'Ü'", "'Ù'", "'Û'");
		$replace = array('a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u');
		$text = preg_replace($pattern, $replace, $text);

		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}
}