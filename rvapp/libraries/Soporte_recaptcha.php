<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Soporte_recaptcha {
	/* Google Recaptcha
	--------------------------- */
	/* Register API keys at https://www.google.com/recaptcha/admin
	   reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language */

	// para cambiar estos datos hay que acceder a Constantes en la configuracion.
	protected $siteKey	= RECAPTCHA_SITEKEY;
	protected $secret	= RECAPTCHA_SECRET;

	function check_recaptcha($post_value) {
		// cargo la libreria
		//$this->load->library('recaptcha/autoload');
		include APPPATH."libraries/recaptcha/autoload.php";
		
		$recaptcha = new \ReCaptcha\ReCaptcha($this->secret);

        $resp = $recaptcha->verify($post_value, $_SERVER['REMOTE_ADDR']);

        if ($resp->isSuccess()) {
        	return true;
        } else {
			// si no ha funcionado, retornamos el codigo de error
			foreach ($resp->getErrorCodes() as $code) {
				//return $code;
			}
			return false;
        }
	}
}
?>