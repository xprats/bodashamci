	<!-- TEXT INTRO -->
	<div id="text-intro">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("g_h3");?></h3>
						<div class="row">
							<div class="col-md-12 subtext">
								<?php echo lang("g_parrafo");?>								
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- /TEXT INTRO -->

	<!-- GALERIA -->
	<div id="qbootstrap-gallery" data-section="gallery">
	
		<div align="center" style="margin-bottom:50px;">
			<button class="btn btn-photos filter-button" id="todas" data-filter="todas"><?php echo lang("g_cat_todas");?></button>
			<button class="btn btn-photos filter-button" id="ceremonia" data-filter="ceremonia"><?php echo lang("g_cat_ceremonia");?></button>
			<button class="btn btn-photos filter-button" id="hotel" data-filter="hotel"><?php echo lang("g_cat_hotel");?></button>
			<button class="btn btn-photos filter-button" id="salones" data-filter="salones"><?php echo lang("g_cat_salones");?></button>
			<button class="btn btn-photos filter-button" id="gastronomia" data-filter="gastronomia"><?php echo lang("g_cat_gastronomia");?></button>
			<button class="btn btn-photos filter-button" id="novios" data-filter="novios"><?php echo lang("g_cat_novios");?></button>
			<button class="btn btn-photos filter-button" id="detalles" data-filter="detalles"><?php echo lang("g_cat_detalles");?></button>		
		</div>
	
		<?php //if (isset($_GET["filter"])) { ?>
			<script	type="text/javascript">
				$(document).ready(function(){
					if (window.location.hash != "") {
						$(window.location.hash).click();
					}
				});
			</script>
		<?php //} ?>

		<div class="container-fluid">
			<?php
				// CEREMONIA
				$limiteCeremonia = 49;
				for ($x = 1; $x <= $limiteCeremonia; $x++) {
					?>
						<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery animate-box filter ceremonia">
							<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-<?php echo $x; ?>.jpg"><img src="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-<?php echo $x; ?>.jpg" class="img-responsive" ></a>
						</div>
					<?php
				}
				
				// HOTEL
				$limiteHotel = 9;
				for ($x = 1; $x <= $limiteHotel; $x++) {
					?>
						<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery animate-box filter hotel">
							<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/hotel/gallery-<?php echo $x; ?>.jpg"><img src="<?php echo base_url(); ?>images/gallery/hotel/gallery-<?php echo $x; ?>.jpg" class="img-responsive" ></a>
						</div>
					<?php
				}

				// SALONES
				$limiteSalones = 8;
				for ($x = 1; $x <= $limiteSalones; $x++) {
					?>
						<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery animate-box filter salones">
							<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/salones/gallery-<?php echo $x; ?>.jpg"><img src="<?php echo base_url(); ?>images/gallery/salones/gallery-<?php echo $x; ?>.jpg" class="img-responsive" ></a>
						</div>
					<?php
				}

				// GASTRONOMIA
				$limiteGastronomia = 45;
				for ($x = 1; $x <= $limiteGastronomia; $x++) {
					?>
						<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery animate-box filter gastronomia">
							<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/gastronomia/gallery-<?php echo $x; ?>.jpg"><img src="<?php echo base_url(); ?>images/gallery/gastronomia/gallery-<?php echo $x; ?>.jpg" class="img-responsive" ></a>
						</div>
					<?php
				}

				// NOVIOS
				$limiteNovios = 24;
				for ($x = 1; $x <= $limiteNovios; $x++) {
					?>
						<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery animate-box filter novios">
							<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/novios/gallery-<?php echo $x; ?>.jpg"><img src="<?php echo base_url(); ?>images/gallery/novios/gallery-<?php echo $x; ?>.jpg" class="img-responsive" ></a>
						</div>
					<?php
				}

				// DETALLES
				$limiteDetalles = 13;
				for ($x = 1; $x <= $limiteDetalles; $x++) {
					?>
						<div class="gallery_product col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery animate-box filter detalles">
							<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/detalles/gallery-<?php echo $x; ?>.jpg"><img src="<?php echo base_url(); ?>images/gallery/detalles/gallery-<?php echo $x; ?>.jpg" class="img-responsive" ></a>
						</div>
					<?php
				}
			?>
		</div>
	</div>
	<!-- /GALERIA -->