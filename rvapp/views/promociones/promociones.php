<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>
    
    <!-- SLIDE -->
    <div class="container-fluid p0">
        <div class="row">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?php echo base_url(); ?>images/slide_promociones_01.jpg">
                        </div>
                    </div>
                </div>
                <div class="main-text">
                    <div class="col-xs-12 text-center">
                        <h1><?php echo lang("promociones_h1");?></h1>
                        <h2><?php echo lang("promociones_h2");?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /SLIDE -->

	<!-- PROMOCIONES Y OFERTAS -->
	<div id="qbootstrap-groom-bride" data-section="promociones">
        <div class="container">
            <div class="row animate-box fadeInUp animated">
                <div class="col-md-12">
                    <div class="col-md-12 text-center section-heading svg-sm-2 colored">
                        <h3><?php echo lang("promociones_h3");?></h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--<div class="col-md-6 col-sm-6 col-xs-12">
                    <?php //$CI->load->view("promociones/minipromo", array("numero" => 3)); ?>
                </div>-->
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php $CI->load->view("promociones/minipromo", array("numero" => 1)); ?>
                </div>  
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php $CI->load->view("promociones/minipromo", array("numero" => 2)); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /PROMOCIONES Y OFERTAS -->
