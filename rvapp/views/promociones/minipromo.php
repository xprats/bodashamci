<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>
    <div class="couple groom text-center animate-box fadeInUp animated">
        <a href="<?php echo base_url().$idioma.lang("promo".$numero."_url");?>"><img src="<?php echo base_url(); ?>images/promo<?php echo $numero;?>.jpg" class="img-responsive"></a>
        <div class="desc">
            <h4><?php echo lang("promo".$numero."_titulo"); ?></h4>
            <p><?php echo lang("promo".$numero."_texto"); ?></p>
        </div>
        <p><a href="<?php echo base_url().$idioma.lang("promo".$numero."_url");?>" class="btn btn-primary btn-sm"><?php echo lang("promo".$numero."_boton"); ?></a></p>
    </div>