<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>
	<!-- PROMOCIONES Y OFERTAS -->
	<div id="qbootstrap-groom-bride" data-section="promociones">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="couple groom text-center animate-box fadeInUp animated">
                        <img src="<?php echo base_url();?>images/promo<?php echo $numero?>.jpg" class="img-responsive">
                        <div class="desc">
                            <h1 class="format_h4"><?php echo lang("promo".$numero."_titulo"); ?></h1>
                            <br>
                            <p><?php echo lang("promo".$numero."_texto"); ?></p>
                            <br>
                            <p><?php echo lang("promo".$numero."_link_menu"); ?></p>
                            <ul>
                                <?php echo lang("promo".$numero."_lista"); ?>
                            </ul>
                            <p style="font-style:italic"><?php echo lang("promo".$numero."_condiciones"); ?></p>
                            <br>
                            <a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-primary btn-sm"><?php echo lang("promo".$numero."_mas_info"); ?></a>
                            <?php echo lang("promo".$numero."_boton_menu"); ?>
                            <br>
                            <br>       
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
    <!-- /PROMOCIONES Y OFERTAS -->
