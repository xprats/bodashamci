	<!-- POLITICA PRIVACIDAD -->
	<div id="legal">
        <div class="container">
            <div class="row animate-box fadeInUp animated">
                <div class="col-md-12">
                    <div class="col-md-12 text-center section-heading svg-sm colored">
                        <h1><?php echo lang("pri_h1"); ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p><strong><?php echo lang("pri_txt_1"); ?></strong></p>
                <p><?php echo lang("pri_txt_2"); ?></p>

                <p><?php echo lang("pri_txt_3"); ?></p>

                <p><?php echo lang("pri_txt_4"); ?></p>

                <p><?php echo lang("pri_txt_5"); ?></p>

                <p><?php echo lang("pri_txt_6"); ?></p>

                <p><?php echo lang("pri_txt_7"); ?></p>

                <p><?php echo lang("pri_txt_8"); ?></p>

                <p><?php echo lang("pri_txt_9"); ?></p>

                <p><?php echo lang("pri_txt_10"); ?></p>

                <p><?php echo lang("pri_txt_11"); ?></p>

                <p><?php echo lang("pri_txt_12"); ?></p>

                <p><?php echo lang("pri_txt_13"); ?></p>

                <p><strong>RV Hotels Turistics, S.L.U.</strong></p>

                <p><strong>NIF:</strong> B82573338</p>

                <p><strong><?php echo lang("pri_txt_14"); ?>:</strong> C/. Diputació, 238. Entl - 3a. Barcelona (08007)</p>

                <p><strong>E-mail:</strong> <a href="mailto:lopd@gruprv.es">lopd@gruprv.es</a></p>

                <p><strong><?php echo lang("pri_txt_15"); ?>:</strong> <a href="tel://+34935036039">(+34) 935 036 039</a></p>
                <br>
                <p><h2>Cookies</h2></p>
                <p><?php echo lang("pri_txt_16"); ?></p>
			
                <p><?php echo lang("pri_txt_17"); ?></p>
            </div>
        </div>
    </div>
    <!-- /POLITICA PRIVACIDAD -->

