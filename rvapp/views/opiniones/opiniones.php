    
    <!-- SLIDE -->
    <div class="container-fluid p0">
        <div class="row">
            <div class="col-md-12">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<?php echo base_url(); ?>images/slide_opiniones_01.jpg">
                        </div>
                    </div>
                </div>
                <div class="main-text">
                    <div class="col-xs-12 text-center">
                        <h1><?php echo lang("o_h1"); ?></h1>
                        <h2><?php echo lang("o_h2"); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /SLIDE -->

	<!-- OPINIONES DE PAREJAS-->
	<div id="qbootstrap-testimonials" class="qbootstrap-greetings">
        <div class="container">
            <div class="row">
				<div class="row animate-box fadeInUp animated" style="padding-bottom:4em">
					<div class="col-md-12">
						<div class="col-md-12 text-center section-heading svg-sm-2 colored">
						</div>
					</div>
				</div>
                <?php 
                    for ($x = 1; $x <= intval(lang("n_opiniones")); $x++) {
                        ?>
                            <div class="col-md-12 animate-box">
                                <div class="box-testimony">
                                    <blockquote>
                                        <span class="quote"><span><i class="far fa-heart"></i></span></span>
                                        <p>&ldquo;<?php echo lang("o_t".$x); ?>&rdquo;</p>
                                    </blockquote>
                                    <p class="author"><?php echo lang("o_f".$x); ?></p>
                                </div>
                            </div>
                        <?php
                    }
                ?>
            </div>
        </div>
    </div>
    <!-- /OPINIONES DE PAREJAS-->
