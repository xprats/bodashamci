<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>

<div class="container-fluid p0">
	<div class="row">
		<div class="col-md-12">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					<li data-target="#carousel-example-generic" data-slide-to="3"></li>
				</ol>
				<div class="carousel-inner">
					<div class="item active"><img src="<?php echo base_url(); ?>images/slide_ceremonia_02.jpg"></div>
					<div class="item"><img src="<?php echo base_url(); ?>images/slide_ceremonia_03.jpg"></div>
					<div class="item"><img src="<?php echo base_url(); ?>images/slide_ceremonia_04.jpg"></div>
					<div class="item"><img src="<?php echo base_url(); ?>images/slide_ceremonia_00.jpg"></div>
				</div>
				<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
				</a>
			</div>
			<div class="main-text hidden-xs">
				<div class="col-xs-12 text-center">
					<h1><?php echo lang("cere_h1");?></h1>
					<h2><?php echo lang("cere_h2");?></h2>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="push"></div>

<!-- TEXT INTRO -->
<div id="text-intro">
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-8 col-md-offset-2">
				<div class="col-md-12 text-center section-heading svg-sm colored">
					<h1 class="visible-xs"><?php echo lang("cere_h1");?></h1>
					<h2 class="visible-xs"><?php echo lang("cere_h2");?></h2>
					<h3><?php echo lang("cere_h3");?></h3>
					<div class="row">
						<div class="col-md-12 subtext">
							<?php echo lang("cere_h2");?>
						</div>
						<div class="col-md-12" style="margin-top:40px;">
							<a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-primary"><?php echo lang("cere_mas_info");?></a>
							<a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("cere_menus");?></a>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /TEXT INTRO -->

<!-- CEREMONIA EN LA PLAYA -->
<div class="col-container animate-box">
	<div class="col">
		<div class="box-text">      
			<h4><?php echo lang("cere_playa_h4");?></h4>
			<p><?php echo lang("cere_playa_desc");?></p>
			<p><?php echo lang("cere_playa_desc2");?></p><br />
			<a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-default"><?php echo lang("cere_mas_info");?></a>
		</div>
	</div>
	<div class="col"><img src="<?php echo base_url(); ?>images/box_ceremonia_01.jpg" class="img-responsive"></div>
</div>
<!-- /CEREMONIA EN LA PLAYA -->

<!-- BODAS ORIGINALES -->
<div class="col-container animate-box hidden-xs">
	<div class="col"><img src="<?php echo base_url(); ?>images/box_ceremonia_02.jpg" class="img-responsive"></div>
	<div class="col">
		<div class="box-text">      
			<h4><?php echo lang("cere_bodas_h4");?></h4>
			<p><?php echo lang("cere_bodas_desc");?></p>
			<p><?php echo lang("cere_bodas_desc2");?></p><br />
			<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#ceremonia" class="btn btn-default"><?php echo lang("cere_galeria");?></a>
		</div>
	</div>
</div>
<!-- BODAS ORIGINALES -->

<!-- BODAS ORIGINALES XS -->
<div class="col-container animate-box visible-xs">
	<div class="col">
		<div class="box-text">      
			<h4><?php echo lang("cere_bodas_h4");?></h4>
			<p><?php echo lang("cere_bodas_desc");?></p>
			<p><?php echo lang("cere_bodas_desc2");?></p><br>
			<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#ceremonia" class="btn btn-default"><?php echo lang("cere_galeria");?></a>
		</div>
	</div>
	<div class="col"><img src="<?php echo base_url(); ?>images/box_ceremonia_02.jpg" class="img-responsive"></div>
</div>
<!-- BODAS ORIGINALES XS -->

<!-- PERSONALIZA TU BODA -->
<div class="col-container animate-box">
	<div class="col">
		<div class="box-text">      
			<h4><?php echo lang("cere_personaliza_h4");?></h4>
			<p><?php echo lang("cere_personaliza_desc");?></p><br>
			<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#detalles" class="btn btn-default"><?php echo lang("cere_galeria");?></a>
		</div>
	</div>
	<div class="col"><img src="<?php echo base_url(); ?>images/box_ceremonia_03.jpg" class="img-responsive"></div>
</div>
<!-- /PERSONALIZA TU BODA -->

<!-- MENUS OK -->
<?php $CI->load->view("gastronomia/menus"); ?>
<!-- /MENUS OK -->

<!-- GALERIA CEREMONIA -->
<div id="qbootstrap-gallery">
	<div class="container">
		<div class="row animate-box">
			<div class="col-md-12">
				<div class="col-md-12 text-center section-heading svg-sm colored">
					<h3><?php echo lang("cere_galeria_h3");?></h3>
					<p><?php echo lang("cere_galeria_desc");?></p><br />
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_01.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_01.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_02.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_02.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_03.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_03.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_04.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_04.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_05.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_05.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_06.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_06.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_07.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_07.jpg" class="img-responsive">
			</a>
		</div>
		<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
			<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_ceremonia_08.jpg">
				<img src="<?php echo base_url(); ?>images/gallery_ceremonia_08.jpg" class="img-responsive">
			</a>
		</div>
		<div class="col-xs-12 text-center">
			<br />
			<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#ceremonia" class="btn btn-default"><?php echo lang("cere_galeria");?></a>
		</div>
	</div>
</div>
<!-- /GALERIA CEREMONIA -->
