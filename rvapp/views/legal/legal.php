    <!-- AVISO LEGAL -->
    <div id="legal">
        <div class="container">
            <div class="row animate-box fadeInUp animated">
                <div class="col-md-12">
                    <div class="col-md-12 text-center section-heading svg-sm colored">
                        <h1><?php echo lang("avi_h1"); ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <p><?php echo lang("avi_txt_1"); ?></p>
                
                <p><?php echo lang("avi_txt_2"); ?></p>
                
                <p><?php echo lang("avi_txt_3"); ?></p>
                
                <p><?php echo lang("avi_txt_4"); ?></p>
            </div>
        </div>
    </div>
    <!-- /AVISO LEGAL -->