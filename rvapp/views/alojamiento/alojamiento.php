<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>
    <!-- SLIDE -->
	<div class="container-fluid p0">
		<div class="row">
			<div class="col-md-12">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<img src="<?php echo base_url(); ?>images/slide_alojamiento_01.jpg">
						</div>
					</div>
				</div>
				<div class="main-text">
					<div class="col-xs-12 text-center">
						<h1><?php echo lang("alo_h1"); ?></h1>
						<h2><?php echo lang("alo_h2"); ?></h2>
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- /SLIDE -->
    
	<!-- TEXT INTRO -->
	<div id="text-intro">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("alo_h3"); ?></h3>
						<div class="row">
							<div class="col-md-12 subtext">
								<?php echo lang("alo_subtext1"); ?> <a href="#" data-nav-section="menus"><strong><?php echo lang("alo_subtext_a"); ?></strong></a> <?php echo lang("alo_subtext2"); ?>
							</div>
							<div class="col-md-12" style="margin-top:40px;">
								<a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-primary"><?php echo lang("alo_mas_info"); ?></a>
                                <a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("alo_menus"); ?></a>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- /TEXT INTRO -->

	<!-- HOTEL PARA INVITADOS -->
	<div class="col-container animate-box">
		<div class="col">
			<div class="box-text">      
				<h4><?php echo lang("alo_hotel_h4"); ?></h4>
				<p><?php echo lang("alo_hotel_desc"); ?></p>
                <p><?php echo lang("alo_hotel_desc2"); ?></p><br>
				<a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-default"><?php echo lang("alo_mas_info"); ?></a>
			</div>
		</div>
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_alojamiento_01.jpg" class="img-responsive">
		</div>
	</div>
	<!-- /HOTEL PARA INVITADOS -->

	<!-- LA SUITE NUPCIAL -->
	<div class="col-container animate-box hidden-xs">
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_alojamiento_02.jpg" class="img-responsive">
		</div>
		<div class="col">
			<div class="box-text">      
				<h4><?php echo lang("alo_suite_h4"); ?></h4>
				<p><?php echo lang("alo_suite_desc"); ?></p>
                <p><?php echo lang("alo_suite_desc2"); ?></p><br>
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#hotel" class="btn btn-default"><?php echo lang("alo_galeria"); ?></a>
			</div>
		</div>
	</div>
	<!-- LA SUITE NUPCIAL -->

	<!-- LA SUITE NUPCIAL XS -->
	<div class="col-container animate-box visible-xs">
		<div class="col">
			<div class="box-text">      
				<h4><?php echo lang("alo_hotel_h4"); ?></h4>
				<p><?php echo lang("alo_suite_desc"); ?></p>
                <p><?php echo lang("alo_suite_desc2"); ?></p><br>
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#hotel" class="btn btn-default"><?php echo lang("alo_galeria"); ?></a>
			</div>
		</div>
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_alojamiento_02.jpg" class="img-responsive">
		</div>
	</div>
	<!-- LA SUITE NUPCIAL XS -->

	<!-- MENUS OK -->
	<?php $CI->load->view("gastronomia/menus"); ?>
	<!-- /MENUS OK -->

	<!-- GALERIA ALOJAMIENTO -->
	<div id="qbootstrap-gallery">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("alo_galeria_h3"); ?></h3>
						<p><?php echo lang("alo_galeria_desc"); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_alojamiento_01.jpg"><img src="<?php echo base_url(); ?>images/gallery_alojamiento_01.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_alojamiento_02.jpg"><img src="<?php echo base_url(); ?>images/gallery_alojamiento_02.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_alojamiento_03.jpg"><img src="<?php echo base_url(); ?>images/gallery_alojamiento_03.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_alojamiento_04.jpg"><img src="<?php echo base_url(); ?>images/gallery_alojamiento_04.jpg" class="img-responsive"></a>
			</div>
			<div class="col-xs-12 text-center">
				<br>
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#hotel" class="btn btn-default"><?php echo lang("alo_galeria"); ?></a>
			</div> 
        </div>   
	</div>
	<!-- /GALERIA ALOJAMIENTO -->
