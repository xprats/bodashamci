<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>  
    
	<!-- FOOTER -->
    <footer id="footer" role="contentinfo">
		<div class="container">
			<div class="row row-bottom-padded-sm">
				<div class="col-md-12 text-center">
					<p>
						Urbanització Roques Daurades, Cala Bon capó S/N. L'Ametlla de Mar, Tarragona.<br>
						<a href="tel:+34977457752" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Telefono', 'event_label': 'Llamada de telefono'});" title="Haz click para llamar - Solo M&oacute;viles"><i class="fas fa-phone"></i> <strong>(+34) 977 457 752</strong></a> | <a href="tel:+34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Telefono', 'event_label': 'Llamada de Movil'});" title="Haz click para llamar - Solo M&oacute;viles"><i class="fas fa-mobile-alt"></i> <strong>(+34) 680 832 316</strong></a> <br> 
						<?php 
							$user_agent = $_SERVER['HTTP_USER_AGENT'];
							if (strpos($user_agent, 'Firefox') == TRUE){
						?>
							<a href="https://web.whatsapp.com/send?phone=34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Chats', 'event_label': 'Whatsapp'});" title="Haz click para enviar un mensaje al n&uacute;mero de Whatsapp - Solo M&oacute;viles"><i class="fab fa-whatsapp"></i> <strong>WhatsApp</strong></a> |
						<?php } else { ?>
                        	<a href="https://api.whatsapp.com/send?phone=34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Chats', 'event_label': 'Whatsapp'});" title="Haz click para enviar un mensaje al n&uacute;mero de Whatsapp - Solo M&oacute;viles"><i class="fab fa-whatsapp"></i> <strong>WhatsApp</strong></a> |
						<?php } ?>
						 <a href="mailto:comercial@hotelametllamar.com" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Correo', 'event_label': 'Envio'});" title="Haz click para enviar un correo electr&oacute;nico"><i class="far fa-envelope"></i> <strong>comercial@hotelametllamar.com</strong></a>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<ul class="social social-circle">
						<li><a href="https://www.instagram.com/bodashotelametllamar/" target="_blank" rel="nofollow"><i class="icon-instagram"></i></a></li>
						<li><a href="https://www.facebook.com/bodasametllamar/" target="_blank" rel="nofollow"><i class="icon-facebook"></i></a></li>
						<li><a href="https://www.youtube.com/channel/UCaYFGdXPEmNpZadftzrgeMQ" target="_blank" rel="nofollow"><i class="icon-youtube"></i></a></li>
					</ul>
				</div>
				<div class="col-md-12 text-center nota-legal" style="font-size:13px;margin-top:20px;">
					<a href="<?php echo base_url().$idioma.lang("m_u_nota_legal");?>">Nota Legal</a> | <a href="<?php echo base_url().$idioma.lang("m_u_privacidad");?>">Política de Privacidad</a>
				</div>	
			</div>
		</div>
	</footer>
    <!-- /FOOTER -->
    
	<!-- jQuery -->
	<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
	<!-- jQuery Easing -->

	<script src="<?php echo base_url(); ?>js/jquery.easing.1.3.js"></script>

	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

	<!-- Waypoints -->
	<script src="<?php echo base_url(); ?>js/jquery.waypoints.min.js"></script>

	<!-- YTPlayer -->
	<script src="<?php echo base_url(); ?>js/jquery.mb.YTPlayer.min.js"></script>

	<!-- Flexslider -->
	<script src="<?php echo base_url(); ?>js/jquery.flexslider-min.js"></script>

	<!-- Owl Carousel -->
	<script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>

	<!-- Parallax -->
	<script src="<?php echo base_url(); ?>js/jquery.stellar.min.js"></script>

	<!-- Magnific Popup -->
	<script src="<?php echo base_url(); ?>js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>js/magnific-popup-options.js"></script>

	<!-- Main JS (Do not remove) -->
	<script src="<?php echo base_url(); ?>js/main.js"></script>

	<script type="text/javascript">
		$(function() {
			var header = $("#qbootstrap-header");
			$(window).scroll(function() {    
				var scroll = $(window).scrollTop();
				if (scroll >= 50) {
					header.addClass("scrolled");
					$('.navbar-brand').attr('src','https://www.bodasametllamar.com/images/logo_ham_scrolled.png');
				} else {
					header.removeClass("scrolled");
					$('.navbar-brand').attr('src','https://www.bodasametllamar.com/images/logo_ham.png');
				}
			});
		});
	</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-45507628-1"></script>
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
	  	function gtag(){dataLayer.push(arguments);}
	  	gtag('js', new Date());
	  	gtag('config', 'UA-45507628-1');
	</script>
	</body>
</html>