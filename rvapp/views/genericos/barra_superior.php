<?php 
	$CI =& get_instance(); 
	$prefijo_promo = (isset($numero)?"promo".$numero."_":"");
?>
	<!-- PREHEADER AZUL -->	
	<div class="pre-header">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-6">
					<div class="select-lang">
						<ul class="list-inline">
							<?php if ($idioma=="ca") { ?>
								<a href="<?php echo lang($prefijo_promo."hreflang_url"); ?>"><li>ES</a>
								<li class="active">CA
							<?php } else { ?>
								<li class="active">ES
								<a href="<?php echo lang($prefijo_promo."hreflang_url"); ?>"><li>CA</a>
							<?php } ?>
						</ul>	
					</div>
				</div>	
				<div class="col-sm-10 hidden-xs">
					<div class="contact text-right">
						<ul class="list-inline">
							<li> <a href="tel://+34977457752" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Telefono', 'event_label': 'Llamada de telefono'});" title="Haz click para llamar - Solo M&oacute;viles"><i class="fas fa-phone"></i> 977 457 752</a></li>
							<li> <a href="tel://+34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Telefono', 'event_label': 'Llamada de Movil'});" title="Haz click para llamar - Solo M&oacute;viles"><i class="fas fa-mobile-alt"></i> 680 832 316</a></li>
							<?php 
								$user_agent = $_SERVER['HTTP_USER_AGENT'];
								if (strpos($user_agent, 'Firefox') == TRUE){
							?>
								<li> <a href="https://web.whatsapp.com/send?phone=34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Chats', 'event_label': 'Whatsapp'});" title="Haz click para enviar un mensaje al n&uacute;mero de Whatsapp - Solo M&oacute;viles"><i class="fab fa-whatsapp"></i> WhatsApp</a></li>
							<?php } else { ?>
                        		<li> <a href="https://api.whatsapp.com/send?phone=34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Chats', 'event_label': 'Whatsapp'});" title="Haz click para enviar un mensaje al n&uacute;mero de Whatsapp - Solo M&oacute;viles"><i class="fab fa-whatsapp"></i> WhatsApp</a></li>
							<?php } ?>
							<li><a href="mailto:comercial@hotelametllamar.com" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Correo', 'event_label': 'Envio'});" title="Haz click para enviar un correo electr&oacute;nico"><i class="far fa-envelope"></i> comercial@hotelametllamar.com</a></li>
						</ul>		
					</div>
				</div>
				<div class="col-xs-6 visible-xs">
					<div class="contact text-right">
						<ul class="list-inline">
							<li> <a href="tel://+34977457752" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Telefono', 'event_label': 'Llamada de telefono'});" title="Haz click para llamar - Solo M&oacute;viles"><i class="fas fa-phone"></i></a></li>
							<li> <a href="tel://+34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Telefono', 'event_label': 'Llamada de Movil'});" title="Haz click para llamar - Solo M&oacute;viles"><i class="fas fa-mobile-alt"></i></a></li>
							<?php 
								$user_agent = $_SERVER['HTTP_USER_AGENT'];
								if (strpos($user_agent, 'Firefox') == TRUE){
							?>
								<li><a href="https://web.whatsapp.com/send?phone=34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Chats', 'event_label': 'Whatsapp'});" title="Haz click para enviar un mensaje al n&uacute;mero de Whatsapp - Solo M&oacute;viles"><i class="fab fa-whatsapp"></i></a></li>
							<?php } else { ?>
                        		<li><a href="https://api.whatsapp.com/send?phone=34680832316" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Chats', 'event_label': 'Whatsapp'});" title="Haz click para enviar un mensaje al n&uacute;mero de Whatsapp - Solo M&oacute;viles"><i class="fab fa-whatsapp"></i></a></li>
							<?php } ?>
							<li><a href="mailto:comercial@hotelametllamar.com" onclick="gtag('event', 'Evento Web', { 'event_category': 'Web', 'event_action': 'Correo', 'event_label': 'Envio'});" title="Haz click para enviar un correo electr&oacute;nico"><i class="far fa-envelope"></i></a></li>
						</ul>		
					</div>
				</div>
			</div>		
		</div>	
	</div>
	<!-- /PREHEADER AZUL -->	
