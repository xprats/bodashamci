<?php
	$prefijo_promo = (isset($numero)?"promo".$numero."_":"");
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo lang($prefijo_promo."titulo"); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo lang($prefijo_promo."meta_description"); ?>" />
	<meta name="author" content="Rosa dels Vents" />

	<link rel="canonical" href="<?php echo lang($prefijo_promo."canonical");?>" />
	<link rel="alternate" hreflang="<?php echo $idioma;?>" href="<?php echo lang($prefijo_promo."canonical");?>" />
	<link rel="alternate" hreflang="<?php echo lang("hreflang_codigo");?>" href="<?php echo lang($prefijo_promo."hreflang_url");?>" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.png">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">
	
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/all.css">

	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->
		
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>js/funcions.js"></script>

	<!-- FONTAWESOME -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

	<!-- PT SANS -->
	<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">

	<!-- RECAPTCHA -->
	<script src='https://www.google.com/recaptcha/api.js'></script>

	</head>
	<body class="<?php echo $css_pagina; ?>">