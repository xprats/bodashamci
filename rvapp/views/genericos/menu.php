<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}    
?>

    <!-- MENU -->	
    <header role="banner" id="qbootstrap-header">
		<div class="container">
			<!-- <div class="row"> -->
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-qbootstrap-nav-toggle qbootstrap-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
					<a href="<?php echo base_url().$idioma; ?>"><img src="<?php echo base_url(); ?>images/logo_ham.png" class="navbar-brand"></a>  
				</div>
				<?php if ($idioma != "") $idioma = $idioma."/"; ?>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
					<li <?php echo ($css_pagina == "ceremonia"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_ceremonia");?>"><?php echo lang("m_t_ceremonia");?></a></li>
					<li <?php echo ($css_pagina == "alojamiento"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_alojamiento");?>"><?php echo lang("m_t_alojamiento");?></a></li>
					<li <?php echo ($css_pagina == "gastronomia"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_gastronomia");?>"><?php echo lang("m_t_gastronomia");?></a></li>
					<li <?php echo ($css_pagina == "galeria"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>"><?php echo lang("m_t_galeria");?></a></li>
					<li <?php echo ($css_pagina == "promociones"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_promociones");?>"><?php echo lang("m_t_promociones");?></a></li>
					<li <?php echo ($css_pagina == "opiniones"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_opiniones");?>"><?php echo lang("m_t_opiniones");?></a></li>
					<li <?php echo ($css_pagina == "preguntas-frecuentes"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_preguntas");?>"><?php echo lang("m_t_preguntas");?></a></li>
					<li <?php echo ($css_pagina == "contacto"?' class="active"':"")?>><a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>"><?php echo lang("m_t_contacto");?></a></li>
					</ul>
				</div>
			</nav>
			<!-- </div> -->
		</div>
	</header>
	<!-- /MENU -->	
<?php /* echo base_url().$idioma.$CI->soporte_xss->slugify(lang("m_nota_legal"));?>
<?php echo base_url().$idioma.$CI->soporte_xss->slugify(lang("m_privacidad")); */?>