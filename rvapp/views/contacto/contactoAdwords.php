<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $lang = "";}
    else  { $lang = $idioma."/";}
?>

    <!-- THANK U -->
	<div id="contacto" class="thank-you">
        <div class="container">
            <div class="row">
				<div class="col-xs-12 text-center contacta animate-box fadeInUp animated">
                <h1 class="amarillo"><?php echo lang("c_adwords_h1")?></h1>
                    <h2><?php echo lang("c_adwords_h2")?></h2>
                    <div class="links">
                        <ul class="list-inline">
                            <a href="tel://+34977457752"><li><i class="fas fa-phone"></i> 977 457 752</a>
                            <a href="tel://+34680832316"><li><i class="fas fa-mobile-alt"></i> 680 832 316</a>
                        </ul>
						<?php 
							$user_agent = $_SERVER['HTTP_USER_AGENT'];
							if (strpos($user_agent, 'Firefox') == TRUE){
						?>
							<a href="https://web.whatsapp.com/send?phone=34680832316"><i class="fab fa-whatsapp"></i> WhatsApp</a><br>
						<?php } else { ?>
                        	<a href="https://api.whatsapp.com/send?phone=34680832316"><i class="fab fa-whatsapp"></i> WhatsApp</a><br>
						<?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /THANK U-->
