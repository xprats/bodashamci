<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $lang = "";}
    else  { $lang = $idioma."/";}
?>

	<!-- CONTACTO-->
	<div id="contacto">
        <div class="container">
            <div class="row">
				<div class="col-md-7 col-sm-6 col-xs-12 text-center contacta">
					<h1 class="amarillo"><?php echo lang("c_h1")?></h1>
                    <h2><?php echo lang("c_h2")?></h2>
                    <h3><?php echo lang("c_h3")?></h3>
                    <div class="links">
                        <ul class="list-inline">
                            <a href="tel://+34977457752"><li><i class="fas fa-phone"></i> 977 457 752</a>
                            <a href="tel://+34680832316"><li><i class="fas fa-mobile-alt"></i> 680 832 316</a>
                        </ul>
						<?php 
							$user_agent = $_SERVER['HTTP_USER_AGENT'];
							if (strpos($user_agent, 'Firefox') == TRUE){
						?>
							<a href="https://web.whatsapp.com/send?phone=34680832316"><i class="fab fa-whatsapp"></i> WhatsApp</a><br>
						<?php } else { ?>
                        	<a href="https://api.whatsapp.com/send?phone=34680832316"><i class="fab fa-whatsapp"></i> WhatsApp</a><br>
						<?php } ?>
                        <a href="mailto:comercial@hotelametllamar.com"><i class="far fa-envelope"></i> comercial@hotelametllamar.com</a>
                    </div>
                    <img src="<?php echo base_url(); ?>images/moto-novios.png" class="moto animate-box fadeInRight animated">
                </div>
                <!-- FORMULARIO -->	
                <div class="col-md-5 col-sm-6 col-xs-12 animate-box formulario">
                    <?php 
                        $attributes = array('accept-charset'=>	"utf-8",
                                            'id'			=>	'form_contacto',
                                            'name'			=> 	'form_contacto',
                                            'autocomplete'	=>	"off");
                        echo form_open('enviar_email', $attributes); 
                        echo form_hidden("idioma", $idioma);
					?>

                        <div class="form-group">
                            <label><?php echo lang("c_nombre")?></label>
                            <input type="name" id="nombre" name="nombre" class="form-control" required value="<?php echo set_value("nombre"); ?>">
                            <?php echo form_error("nombre");?>
                        </div>
                        <div class="form-group">
                            <label><?php echo lang("c_email")?></label>
                            <input type="email" id="email" name="email" class="form-control" required value="<?php echo set_value("email"); ?>">
                            <?php echo form_error("email");?>
                        </div>
                        <div class="form-group">
                            <label><?php echo lang("c_telefono")?></label>
                            <input type="phone" id="telefono" name="telefono" class="form-control" required value="<?php echo set_value("telefono"); ?>">
                            <?php echo form_error("telefono");?>
                        </div>
                        <div class="form-group">
                            <label><?php echo lang("c_comentarios")?></label>
                            <textarea class="form-control" id="comentarios" name="comentarios" rows="3"><?php echo set_value("comentarios"); ?></textarea>
                            <?php echo form_error("comentarios");?>
                        </div>
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="privacidad" name="privacidad" value="1" required>
                            <label class="form-check-label"><?php echo lang("c_privacidad_1"); ?> <a href="<?php echo base_url().$lang.lang("m_u_privacidad"); ?>"> <?php echo lang("c_privacidad_2")?></a></label>
                            <?php echo form_error("privacidad");?>
                        </div>
						<div class="g-recaptcha" data-sitekey="6Ld7y2oUAAAAAA5SCSq495FsUHYOKY5R8_8W5B7V"></div>
                        <div style="padding-top:1em">
							<!--<button type="submit" class="btn btn-primary g-recaptcha btn-block" data-sitekey="6LeEZWoUAAAAAMRxH0iOe4_RP3Dc3C4dBzr0lWay" data-callback="YourOnSubmitFn">Enviar</button>-->
							<button type="submit" class="btn btn-primary btn-block">Enviar</button>
                        </div>
                    </form>
                </div>
                <!-- /FORMULARIO -->
            </div>
        </div>
    </div>
    <!-- /CONTACTO-->
