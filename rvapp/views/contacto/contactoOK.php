<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}

?>

	<!-- THANK U -->

	<div id="contacto" class="thank-you">
        <div class="container">
            <div class="row">
				<div class="col-xs-12 text-center contacta animate-box fadeInUp animated">
					<h1 class="amarillo"><?php echo lang("c_h1"); ?></h1>
                    <h2><?php echo lang("c_ok_h2"); ?></h2>
                    <br>
					<a href="<?php echo base_url().$idioma; ?>" class="btn btn-primary"><?php echo lang("c_ok_volver"); ?></a>
                </div>
            </div>
        </div>
    </div>
    <!-- /THANK U-->	