<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>
    <!-- MENUS -->
    <div class="container-fluid p0" data-section="menus"> <!-- data-section="menus" -->
		<div class="menus">
			<div class="row">
				<div class="col-md-5 col-sm-12 col-xs-12">
					<div class="title-amarillo text-center">
						<p><?php echo lang("menus_titulo"); ?></p>
					</div>
				</div>
				<a href="<?php echo lang("menu1_link_menu"); ?>" target="_blank">
					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="menu-item la-cala border">
							<div class="bg-title text-center">
                                <?php echo lang("menu1_titulo"); ?>
							</div>
							<div class="foto-menu">
								<img src="<?php echo base_url(); ?>images/bg_la_cala.jpg" class="img-responsive">
							</div>
							<div class="btn-menu text-center">
                                <?php echo lang("menu1_info1"); ?>								
							</div>
							<div class="text-center">
                                <?php echo lang("menu1_info2"); ?>
							</div>
							<div class="precio-menu text-center">
                                <?php echo lang("menu1_desde"); ?>
							</div>
						</div>	
					</div>
				</a>
				<a href="<?php echo lang("menu2_link_menu"); ?>" target="_blank">
					<div class="col-md-3 col-sm-6 col-xs-12 animate-box">
						<div class="menu-item aguamarina border">
							<div class="bg-title text-center">
                                <?php echo lang("menu2_titulo"); ?>
							</div>
							<div class="foto-menu">
								<img src="<?php echo base_url(); ?>images/bg_aguamarina.jpg" class="img-responsive">
							</div>
							<div class="text-center">
                                <?php echo lang("menu2_info1"); ?>
                            </div>
							<div class="text-center">
                                <?php echo lang("menu2_info2"); ?>
							</div>
							<div class="precio-menu text-center">
                                <?php echo lang("menu2_desde"); ?>
							</div>
						</div>	
					</div>
				</a>
			</div>
		</div>
	</div>
	<!-- /MENUS -->