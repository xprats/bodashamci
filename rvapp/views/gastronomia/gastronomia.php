<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>
	
	<div class="container-fluid p0">
		<div class="row">
			<div class="col-md-12">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<img src="<?php echo base_url(); ?>images/slide_gastronomia_01.jpg">
						</div>
					</div>
				</div>
				<div class="main-text">
					<div class="col-xs-12 text-center">
						<h1><?php echo lang("gas_h1"); ?></h1>
						<h2><?php echo lang("gas_h2"); ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="push">
	</div>

	<!-- TEXT INTRO -->
	<div id="text-intro">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("gas_h3"); ?></h3>
						<div class="row">
							<div class="col-md-12 subtext">
								<?php echo lang("gas_subtext"); ?>							
							</div>
							<div class="col-md-12" style="margin-top:40px;">
                                <a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-primary"><?php echo lang("gas_mas_info"); ?></a>
                                <a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("gas_menus"); ?></a>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- /TEXT INTRO -->

	<!-- APERITIVOS -->
	<div class="col-container animate-box">
		<div class="col">
			<div class="box-text">      
                <h4><?php echo lang("gas_aperitivos_h4"); ?></h4>
                <p><?php echo lang("gas_aperitivos_desc"); ?></p><br>
				<a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("gas_menus"); ?></a>
			</div>
		</div>
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_gastronomia_01.jpg" class="img-responsive">
		</div>
	</div>
	<!-- /APERITIVOS -->

	<!-- GASTRONOMIA -->
	<div class="col-container animate-box hidden-xs">
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_gastronomia_02.jpg" class="img-responsive">
		</div>
		<div class="col">
			<div class="box-text">      
				<h4><?php echo lang("gas_gastronomia_h4"); ?></h4>
				<p><?php echo lang("gas_gastronomia_desc"); ?></p><br>
				<br>
				<a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("gas_menus"); ?></a>
			</div>
		</div>
	</div>
	<!-- /GASTRONOMIA -->
	
	<!-- GASTRONOMIA XS -->
	<div class="col-container animate-box visible-xs">
		<div class="col">
			<div class="box-text">      
				<h4><?php echo lang("gas_gastronomia_h4"); ?></h4>
				<p><?php echo lang("gas_gastronomia_desc"); ?></p><br>
				<p><?php echo lang("gas_gastronomia_desc2"); ?></p><br>
				<a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("gas_menus"); ?></a>
			</div>
		</div>
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_gastronomia_02.jpg" class="img-responsive">
		</div>
	</div>
	<!-- /GASTRONOMIA XS-->
    
    <!-- SALON BANQUETES -->
	<div class="col-container animate-box">
        <div class="col">
            <div class="box-text">      
				<h4><?php echo lang("gas_banquetes_h4"); ?></h4>
				<p><?php echo lang("gas_banquetes_desc"); ?></p><br>
				<p><?php echo lang("gas_banquetes_desc2"); ?></p><br>
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#salones" class="btn btn-default"><?php echo lang("gas_galeria"); ?></a>
            </div>
        </div>
        <div class="col">
            <img src="<?php echo base_url(); ?>images/box_gastronomia_03.jpg" class="img-responsive">
        </div>
    </div>
    <!-- /SALON BANQUETES -->

    <!-- DISCO -->
	<div class="col-container animate-box hidden-xs">
        <div class="col">
            <img src="<?php echo base_url(); ?>images/box_gastronomia_04.jpg" class="img-responsive">
        </div>
        <div class="col">
            <div class="box-text">      
				<h4><?php echo lang("gas_disco_h4"); ?></h4>
				<p><?php echo lang("gas_disco_desc"); ?></p><br>
				<p><?php echo lang("gas_disco_desc2"); ?></p><br>
				<a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("gas_menus"); ?></a>
            </div>
        </div>
    </div>
	<!-- /DISCO -->
	
	<!-- DISCO XS -->
	<div class="col-container animate-box visible-xs">
		<div class="col">
			<div class="box-text">      
				<h4><?php echo lang("gas_disco_h4"); ?></h4>
				<p><?php echo lang("gas_disco_desc"); ?></p><br>
				<p><?php echo lang("gas_disco_desc2"); ?></p><br>
				<a href="#" data-nav-section="menus" class="btn btn-default"><?php echo lang("gas_menus"); ?></a>
			</div>
		</div>
		<div class="col">
			<img src="<?php echo base_url(); ?>images/box_gastronomia_04.jpg" class="img-responsive">
		</div>
	</div>
	<!-- /DISCO XS -->

	<!-- MENUS OK -->
	<?php $CI->load->view("gastronomia/menus"); ?>
	<!-- /MENUS OK -->

	<!-- GALERIA GASTRONOMIA -->
	<div id="qbootstrap-gallery">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("gas_galeria_h3"); ?></h3>
						<p><?php echo lang("gas_galeria_desc"); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_01.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_01.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_02.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_02.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_03.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_03.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_04.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_04.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_05.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_05.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_06.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_06.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_07.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_07.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_08.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_08.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_09.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_09.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_10.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_10.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_11.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_11.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_gastronomia_12.jpg"><img src="<?php echo base_url(); ?>images/gallery_gastronomia_12.jpg" class="img-responsive"></a>
			</div>
			<div class="col-xs-12 text-center">
				<br>	
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#gastronomia" class="btn btn-default"><?php echo lang("gas_galeria"); ?></a>
			</div> 
        </div>   
	</div>
	<!-- /GALERIA GASTRONOMIA -->
