    
	<!-- PREGUNTAS FRECUENTES-->
	<div id="faq">
        <div class="container">
            <div class="row">
				<div class="col-md-12 text-center" style="padding-bottom:3em;">
					<h1><?php echo lang("p_h1"); ?></h1>
					<h2><?php echo lang("p_h2"); ?></h2>
				</div>	
                <div class="col-md-8 animate-box">
					<div class="fancy-collapse-panel">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php 
								$collapsed = " in";
								for ($x = 1; $x <= intval(lang("n_preguntas")); $x++) {
									?>
										<div class="panel panel-default">
											<div class="panel-heading" role="tab" id="heading<?php echo $x; ?>">
												<h3 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $x; ?>" aria-expanded="true" aria-controls="collapse<?php echo $x; ?>"><?php echo lang("p_p".$x); ?></a>
												</h3>
											</div>
											<div id="collapse<?php echo $x; ?>" class="panel-collapse collapse<?php echo $collapsed?>" role="tabpanel" aria-labelledby="heading<?php echo $x; ?>">
												<div class="panel-body">
													<?php echo lang("p_r".$x); ?>
												</div>
											</div>
										</div>
									<?php
									$collapsed = "";
								}
							?>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    <!-- /PREGUNTAS FRECUENTES-->
