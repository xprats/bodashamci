<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. */
    $CI =& get_instance(); 
    if ($idioma == "es") { $idioma = "";}
    else  { $idioma = $idioma."/";}
?>  

	<!-- VIDEO -->
	<section id="home" class="video-hero" style="background-color: #2980b9; background-size:cover; background-position: center center;background-attachment:fixed;">
	<div class="qbootstrap-overlay"></div>
	<a class="player" data-property="{videoURL:'https://youtu.be/aNHCj9ERMmw',containment:'#home', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, quality:'default'}"></a>  
		<div class="display-t text-center">
			<div class="display-tc">
				<div class="container">
					<div class="col-md-10 col-md-offset-1">
						<div class="animate-box title-home">
							<h1><?php echo lang("hom_h1"); ?></h1>
							<h2><?php echo lang("hom_h2"); ?></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /VIDEO -->

	<!-- MINI SECCIONES HOME -->
	<div id="qbootstrap-press">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("hom_h3"); ?></h3>
						<div class="row">
							<div class="col-md-12 subtext">
								<p><?php echo lang("hom_subtext"); ?></p>
							</div>
							<div class="col-md-12" style="margin-top:40px;">
								<a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-default" data-nav-section="rsvp"><?php echo lang("hom_btn_casar_playa"); ?></a>
							</div>
						</div>	
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="qbootstrap-press-item animate-box">
						<div class="qbootstrap-press-img" style="background-image: url(<?php echo base_url(); ?>images/home_01.jpg)">
						</div>
						<div class="qbootstrap-press-text">
							<h3 class="h2 qbootstrap-press-title"><?php echo lang("hom_tit_box1"); ?></h3>
							<p><?php echo lang("hom_txt_box1"); ?></p><br />
							<a href="<?php echo base_url().$idioma.lang("m_u_ceremonia");?>" class="btn btn-primary btn-sm" data-nav-section="rsvp"><?php echo lang("hom_mas_info"); ?></a>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="qbootstrap-press-item animate-box">
						<div class="qbootstrap-press-img" style="background-image: url(<?php echo base_url(); ?>images/home_02.jpg)">
						</div>
						<div class="qbootstrap-press-text">
							<h3 class="h2 qbootstrap-press-title"><?php echo lang("hom_tit_box2"); ?></span></h3>
							<p><?php echo lang("hom_txt_box2"); ?></p><br />
							<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>#detalles" class="btn btn-primary btn-sm" data-nav-section="rsvp"><?php echo lang("hom_mas_info"); ?></a>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="qbootstrap-press-item animate-box">
						<div class="qbootstrap-press-img" style="background-image: url(<?php echo base_url(); ?>images/home_03.jpg);">
						</div>
						<div class="qbootstrap-press-text">
							<h3 class="h2 qbootstrap-press-title"><?php echo lang("hom_tit_box3"); ?></span></h3>
							<p><?php echo lang("hom_txt_box3"); ?></p><br />
							<a href="<?php echo base_url().$idioma.lang("m_u_alojamiento");?>" class="btn btn-primary btn-sm" data-nav-section="rsvp"><?php echo lang("hom_mas_info"); ?></a>
						</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="qbootstrap-press-item animate-box">
						<div class="qbootstrap-press-img" style="background-image: url(<?php echo base_url(); ?>images/home_04.jpg);">
						</div>
						<div class="qbootstrap-press-text">
							<h3 class="h2 qbootstrap-press-title"><?php echo lang("hom_tit_box4"); ?></span></h3>
							<p><?php echo lang("hom_txt_box4"); ?></p><br />
							<a href="<?php echo base_url().$idioma.lang("m_u_gastronomia");?>" class="btn btn-primary btn-sm" data-nav-section="rsvp"><?php echo lang("hom_mas_info"); ?></a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- /MINI SECCIONES HOME -->

	<!-- CALL TO ACTION -->
	<div id="qbootstrap-organize">
		<div class="container">
			<div class="col-xs-12 text-center">
				<p><strong>"<?php echo lang("hom_txt_blue_box"); ?>"</strong></p><br />
			</div>
			<div class="col-xs-12 text-center">
				<p><a href="<?php echo base_url().$idioma.lang("m_u_contacto");?>" class="btn btn-light"><?php echo lang("hom_btn_org_boda"); ?></a></p>
			</div>
		</div>
	</div>
	<!-- /CALL TO ACTION -->

	<!-- GALERIA HOME -->
	<!--div id="qbootstrap-gallery">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("hom_fotos_h3"); ?></h3>
						<p><?php echo lang("hom_fotos_txt"); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_01.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_01.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_02.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_02.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_03.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_03.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_04.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_04.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_05.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_05.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_06.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_06.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_07.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_07.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery_home_08.jpg"><img src="<?php echo base_url(); ?>images/gallery_home_08.jpg" class="img-responsive"></a>
			</div>
			<div class="col-xs-12 text-center">
				<br>
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>" class="btn btn-default"><?php echo lang("hom_btn_ver_fotos"); ?></a>
			</div> 
		</div>   
	</div-->
	<!-- /GALERIA HOME -->

<!-- GALERIA HOME -->
	<div id="qbootstrap-gallery">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<h3><?php echo lang("hom_fotos_h3"); ?></h3>
						<p><?php echo lang("hom_fotos_txt"); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/novios/gallery-11.jpg"><img src="<?php echo base_url(); ?>images/gallery/novios/gallery-11.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-5.jpg"><img src="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-5.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-6.jpg"><img src="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-6.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-7.jpg"><img src="<?php echo base_url(); ?>/images/gallery/ceremonia/gallery-7.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-8.jpg"><img src="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-8.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-9.jpg"><img src="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-9.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-10.jpg"><img src="<?php echo base_url(); ?>images/gallery/ceremonia/gallery-10.jpg" class="img-responsive"></a>
			</div>
			<div class="gallery_product col-lg-3 col-md-3 col-sm-6 col-xs-12 gallery animate-box filter fadeInUp animated">
				<a class="gallery-img image-popup" href="<?php echo base_url(); ?>images/gallery/novios/gallery-10.jpg"><img src="<?php echo base_url(); ?>images/gallery/novios/gallery-10.jpg" class="img-responsive"></a>
			</div>
			<div class="col-xs-12 text-center">
				<br>
				<a href="<?php echo base_url().$idioma.lang("m_u_galeria");?>" class="btn btn-default"><?php echo lang("hom_btn_ver_fotos"); ?></a>
			</div> 
		</div>   
	</div>
	<!-- /GALERIA HOME -->

	<!-- KEY SELLING POINTS -->
	<div id="qbootstrap-ksp">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12 col-sm-12 col-xs-12 text-center">
					<h3><?php echo lang("hom_puntos_h3"); ?></h3>
					<p class="frase"><?php echo lang("hom_puntos_txt"); ?></p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 animate-box">
					<div class="ksp">
						<ul>
							<li><?php echo lang("hom_punto_1"); ?></li>
							<li><?php echo lang("hom_punto_2"); ?></li>
							<li><?php echo lang("hom_punto_3"); ?></li>
							<li><?php echo lang("hom_punto_4"); ?></li>
						</ul>		
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 animate-box">
					<div class="ksp">
						<ul>
							<li><?php echo lang("hom_punto_5"); ?></li>
							<li><?php echo lang("hom_punto_6"); ?></li>
							<li><?php echo lang("hom_punto_7"); ?></li>
							<li><?php echo lang("hom_punto_8"); ?></li>
						</ul>		
					</div>
				</div>
				<div class="col-md-12 col-sm-12 col-xs-12 animate-box">
					<div class="ksp">
						<ul>
							<li><?php echo lang("hom_punto_9"); ?></li>
						</ul>		
					</div>
				</div>
			</div>	
		</div>
	</div>
	<!-- /KEY SELLING POINTS -->

	<!-- PROMOCIONES Y OFERTAS -->
	<div id="qbootstrap-groom-bride" data-section="promociones">
		<div class="container">
			<div class="row animate-box fadeInUp animated">
				<div class="col-md-12">
					<div class="col-md-12 text-center section-heading svg-sm-2 colored">
						<h3><?php echo lang("hom_promos_h3"); ?></h3>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
                    <?php $CI->load->view("promociones/minipromo", array("numero" => 1)); ?>
                </div>  
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php $CI->load->view("promociones/minipromo", array("numero" => 2)); ?>
                </div>
				<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
					<p style="text-align: center; padding:20px;"><?php echo lang("hom_promos_txt"); ?><br /><br><a href="<?php echo base_url().$idioma.lang("m_u_promociones");?>" class="btn btn-default btn-sm"><?php echo lang("hom_btn_ver_promos"); ?></a></p>
				</div>
			</div>
		</div>
	</div>
	<!-- /PROMOCIONES Y OFERTAS -->

	<!-- OPINIONES DE PAREJAS-->
	<div id="qbootstrap-testimonials" class="qbootstrap-greetings" data-section="greetings" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo base_url(); ?>images/cover_bg_1.jpg);">
	<div class="overlay"></div>	
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-12 section-heading text-center svg-sm colored">
					<h3 class=""><?php echo lang("hom_opi_h3"); ?></h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 animate-box">
					<div class="box-testimony ">
						<blockquote>
							<span class="quote"><span><i class="far fa-heart"></i></span></span>
							<p>&ldquo;<?php echo lang("hom_opi_1"); ?>&rdquo;</p>
						</blockquote>
						<p class="author">Marina &amp; Kiko<br>29-09-2018</p>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="box-testimony ">
						<blockquote>
							<span class="quote"><span><i class="far fa-heart"></i></i></span></span>
							<p>&ldquo;<?php echo lang("hom_opi_2"); ?>&rdquo;</p>
						</blockquote>
						<p class="author">Saray &amp; Cisco<br>8-06-2018</p>
					</div>
				</div>
				<div class="col-md-4 animate-box">
					<div class="box-testimony ">
						<blockquote>
							<span class="quote"><span><i class="far fa-heart"></i></i></span></span>
							<p>&ldquo;<?php echo lang("hom_opi_3"); ?>&rdquo;</p>
						</blockquote>
						<p class="author">Nerea &amp; Josep Ramón<br>25-05-2018</p>
					</div>
				</div>
				<div class="col-xs-12 text-center" style="margin:40px 0px;">
					<a href="<?php echo base_url().$idioma.lang("m_u_opiniones");?>" class="btn btn-default" data-nav-section="rsvp"><?php echo lang("hom_btn_ver_opi"); ?></a>
				</div>
			</div>
		</div>
	</div>
	<!-- /OPINIONES DE PAREJAS-->
