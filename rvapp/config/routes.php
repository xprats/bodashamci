<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// ES
    $route['boda-en-la-playa']                      = 'ceremonia/index';
    $route['alojamiento']                           = 'alojamiento/index';
    $route['gastronomia-espacios']                  = 'gastronomia/index';
    $route['galeria']                               = 'galeria/index';
    $route['promociones']                           = 'promociones/index';
    $route['opiniones-de-parejas']                  = 'opiniones/index';
    $route['preguntas-frecuentes']                  = 'preguntas/index';
    $route['contacto']                              = 'contacto/index';
    $route['nota-legal']                            = 'legal/index';
    $route['politica-de-privacidad']                = 'privacidad/index';

    $route['boda-en-la-playa/(:any)']               = 'ceremonia/index/$1';
    $route['alojamiento/(:any)']                    = 'alojamiento/index/$1';
    $route['gastronomia-espacios/(:any)']           = 'gastronomia/index/$1';
    $route['galeria/(:any)']                        = 'galeria/index/$1';
    $route['promociones/(:any)']                    = 'promociones/index/$1';
    $route['opiniones-de-parejas/(:any)']           = 'opiniones/index/$1';
    $route['preguntas-frecuentes/(:any)']           = 'preguntas/index/$1';
    $route['contacto/(:any)']                       = 'contacto/index/$1';
    $route['nota-legal/(:any)']                     = 'legal/index/$1';
    $route['politica-de-privacidad/(:any)']         = 'privacidad/index/$1';

    $route['home/(:any)']                           = 'home/index/$1';
    $route['home/cambiar_idioma/(:any)']            = 'home/index/$1';


// CA
    $prefijo = "ca/";
    $route[$prefijo.'casament-a-la-platja']                 = 'ceremonia/index';
    $route[$prefijo.'allotjament']                          = 'alojamiento/index';
    $route[$prefijo.'gastronomia-espais']                   = 'gastronomia/index';
    $route[$prefijo.'galeria']                              = 'galeria/index';
    $route[$prefijo.'promocions']                           = 'promociones/index';
    $route[$prefijo.'opinions-de-parelles']                 = 'opiniones/index';
    $route[$prefijo.'preguntes-frequents']                  = 'preguntas/index';
    $route[$prefijo.'contacte']                             = 'contacto/index';
    $route[$prefijo.'nota-legal']                           = 'legal/index';
    $route[$prefijo.'politica-de-privacitat']               = 'privacidad/index';

    $route[$prefijo.'casament-a-la-platja/(:any)']          = 'ceremonia/index/$1';
    $route[$prefijo.'allotjament/(:any)']                   = 'alojamiento/index/$1';
    $route[$prefijo.'gastronomia-espais/(:any)']            = 'gastronomia/index/$1';
    $route[$prefijo.'galeria/(:any)']                       = 'galeria/index/$1';
    $route[$prefijo.'promocions/(:any)']                    = 'promociones/index/$1';
    $route[$prefijo.'opinions-de-parelles/(:any)']          = 'opiniones/index/$1';
    $route[$prefijo.'preguntes-frequents/(:any)']           = 'preguntas/index/$1';
    $route[$prefijo.'contacte/(:any)']                      = 'contacto/index/$1';
    $route[$prefijo.'nota-legal/(:any)']                    = 'legal/index/$1';
    $route[$prefijo.'politica-de-privacitat/(:any)']        = 'privacidad/index/$1';

    $route[$prefijo.'home/(:any)']                          = 'home/index/$1';
    $route[$prefijo.'home/cambiar_idioma/(:any)']           = 'home/index/$1';

// PROMOS: 

// promo1:
    $route['alojamiento-gratuito-invitados']                = 'promociones/ver/1';
    $route['alojamiento-gratuito-invitados/(:any)']         = 'promociones/ver/1/$1';
    $route[$prefijo.'allotjament-gratuit-convidats']        = 'promociones/ver/1';
    $route[$prefijo.'allotjament-gratuit-convidats/(:any)'] = 'promociones/ver/1/$1';
// / promo1

// promo2:
    $route['promocion-bodas-sabados']                       = 'promociones/ver/2';
    $route['promocion-bodas-sabados/(:any)']                = 'promociones/ver/2/$1';
    $route[$prefijo.'promocio-casaments-dissabte']          = 'promociones/ver/2';
    $route[$prefijo.'promocio-casaments-dissabte/(:any)']   = 'promociones/ver/2/$1';
// / promo2

// promo3:
    $route['promocion-casate-junio']                       = 'promociones/ver/3';
    $route['promocion-casate-junio/(:any)']                = 'promociones/ver/3/$1';
    $route[$prefijo.'promocio-casat-juny']          = 'promociones/ver/3';
    $route[$prefijo.'promocio-casat-juny/(:any)']   = 'promociones/ver/3/$1';
// / promo3

// resto
    $route['enviar_email']                                        = 'contacto/lanzar';
    $route[$prefijo.'enviar_email']                               = 'contacto/lanzar';
    $route["ca"]                                                  = 'home';
    // $route['(:any)']                                = 'home/index/$1';

    // rutas ok contacto basico
    $route['gracias-por-contactar-con-nosotros']                  = 'contacto/ok';
    $route['gracias-por-contactar-con-nosotros/(:any)']           = 'contacto/ok';
    $route[$prefijo.'gracies-per-contactar-amb-nosaltres']        = 'contacto/ok';
    $route[$prefijo.'gracies-per-contactar-amb-nosaltres/(:any)'] = 'contacto/ok';
 
    // rutas ok contacto mailing
    $route['hemos-recibido-tu-peticion']                  = 'contacto/okmailing';
    $route['hemos-recibido-tu-peticion/(:any)']           = 'contacto/okmailing';
    $route[$prefijo.'hem-rebut-la-teva-peticio']        = 'contacto/okmailing';
    $route[$prefijo.'hem-rebut-la-teva-peticio/(:any)'] = 'contacto/okmailing';

    // rutas adwords
    $route['entrada-gratis-feria-tot-nuvis']                  = 'contacto/okadwords';
    $route['entrada-gratis-feria-tot-nuvis/(:any)']           = 'contacto/okadwords';
 

