// FUNCIÓ DE LA GALERIA

jQuery(document).ready(function(){
	"use strict";
	jQuery.fn.extend({
		animateCss: function(animationName, callback) {
			var animationEnd = (function(el) {
				var animations = {
					animation:       'animationend',
					OAnimation:      'oAnimationEnd',
					MozAnimation:    'mozAnimationEnd',
					WebkitAnimation: 'webkitAnimationEnd',
				};
				for (var t in animations) {
					if (el.style[t] !== undefined) {
						return animations[t];
					}
				}
			})(document.createElement('div'));
			this.addClass('animated ' + animationName).one(animationEnd, function() {
				jQuery(this).removeClass('animated ' + animationName);
				if (typeof callback === 'function') { callback(); }
			});
			return this;
		},
	});
	
    jQuery(".filter-button").click(function(){
        var value = jQuery(this).attr('data-filter');
        if(value === "todas"){
            jQuery('.filter').show('1000');
			jQuery('.filter').removeClass("fadeInUp").removeClass("animated").addClass("fadeInUp").addClass("animated");
        }
        else{
            jQuery(".filter").not('.'+value).hide('1000');
			jQuery(".filter").not('.'+value).removeClass("fadeInUp").removeClass("animated");
            jQuery('.filter').filter('.'+value).show('3000');
			jQuery(".filter").filter('.'+value).removeClass("fadeInUp").removeClass("animated").addClass("fadeInUp").addClass("animated");
        }
    });
    if (jQuery(".filter-button").removeClass("active")) {
		jQuery(this).removeClass("active");
	}
	jQuery(this).addClass("active");
});

// FUNCIÓ DEL FILTRE DELS BOTONS DE LA GALERIA

$(document).ready(function(){
	"use strict";
	console.log("entramos a pulsar el boton");
	if (typeof window.filtro !== 'undefined') {
		console.log("boton "+window.filtro);
		$(window.filtro).click();
	}
});